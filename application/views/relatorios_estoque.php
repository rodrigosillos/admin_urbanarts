<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Administrador Urban Arts - Relatório de Estoque</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<?php include_once('includes/menu.php'); ?>
			<h1>Relatório de Estoque</h1>
			<div class="row">
				<div class="col-md-8">
					<form name="form_relatorio_estoque" method="post" action="/relatorios/gerar_excel_estoque" enctype="multipart/form-data">
						<div class="form-group">
							<label for="planilha_excel">Planilha Excel ( MILLENNIUM / RELATORIOS INTERNOS / 012 - ESTOQUE DISPONIVEL )</label>
							<input type="file" id="planilha_excel" name="planilha_excel">
							<p class="help-block">.xls .xlsx</p>
						</div>
						<div class="form-group">
							<label>Tamanhos</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="ima_15x15" value="1"> Imã 15x15
							</label>
						</div>						
						<div class="checkbox">
							<label>
								<input type="checkbox" name="poster_20x20" value="1"> Poster 20x20
							</label>
						</div>						
						<div class="checkbox">
							<label>
								<input type="checkbox" name="poster_30x30" value="1"> Poster 30x30
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="poster_p" value="1"> Poster P
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="poster_g" value="1"> Poster G
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="filete_20x20" value="1"> Filete 20x20
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="filete_30x30" value="1"> Filete 30x30
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="filete_p" value="1"> Filete P
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="filete_g" value="1"> Filete G
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="sobmedida" value="1"> Sob Medida
							</label>
						</div>						
						<button type="submit" class="btn btn-default">Gerar</button>
					</form>
				</div>
			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="/assets/js/engine_menu.js"></script>
		<script src="/assets/js/engine_relatorio.js"></script>
		<script type="text/javascript">
		<?php if($this->session->flashdata('error_upload')){ ?>
		alert("<?php echo $this->session->flashdata('error_upload'); ?>");
		<?php } ?>
		</script>
	</body>
</html>