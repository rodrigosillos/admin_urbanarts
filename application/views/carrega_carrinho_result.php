<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Administrador Urban Arts - Pedidos / Carrinho</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<?php include_once('includes/menu.php'); ?>
			<h2>Carrinho carregado com sucesso!</h2>
			<div class="row">
				<div class="col-md-8">
					<?php if(isset($error_carrinho) && sizeof($error_carrinho)>0){ echo 'Erros: <br/><br/><pre>'; print_r($error_carrinho); } ?>
				</div>
			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="/assets/js/engine_menu.js"></script>
		<script type="text/javascript">
		<?php if($this->session->flashdata('error_upload')){ ?>
		alert("<?php echo $this->session->flashdata('error_upload'); ?>");
		<?php } ?>
		</script>
	</body>
</html>