<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Administrador Urban Arts - Relatório de Reposição</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<?php include_once('includes/menu.php'); ?>
			<h1>Relatório de Reposição</h1>
			<div class="row">
				<div class="col-md-7">
					<form name="form_upload_import_reposicao" id="form_upload_import_reposicao" target="target_upload_import" method="post" action="/relatorios/upload_import_reposicao" enctype="multipart/form-data">
						<div class="form-group">
							<label for="planilha_excel">Planilha Excel ( MILLENNIUM / RELATORIOS INTERNOS / 005 - RANKING PRODUTOS )</label>
							<input type="file" id="planilha_excel" name="planilha_excel">
							<p class="help-block">.xls .xlsx</p>
						</div>
						<button type="submit" class="btn btn-default">Carregar</button>
					</form>
					<div id="target_upload_import"></div>
					<hr size="1">
					<form name="form_relatorio_reposicao" method="post" action="/relatorios/gerar_excel_reposicao" enctype="multipart/form-data">
						<div class="form-group">
							<label for="tipo_pedido">Tipo de Pedido</label>
							<select class="form-control" id="tipo_pedido" name="tipo_pedido">
								<option value="PRONTA-ENTREGA">PRONTA-ENTREGA</option>
								<option value="ENCOMENDA">ENCOMENDA</option>
							</select>
						</div>
						<div class="form-group">
							<label>Tamanhos</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="ima_15x15" value="1"> <b>Imã 15x15</b> <div id="reposicao_ima_15x15"></div>
							</label>
						</div>						
						<div class="checkbox">
							<label>
								<input type="checkbox" name="poster_20x20" value="1"> <b>Poster 20x20</b> <div id="reposicao_poster_20x20"></div>
							</label>
						</div>						
						<div class="checkbox">
							<label>
								<input type="checkbox" name="poster_30x30" value="1"> <b>Poster 30x30</b> <div id="reposicao_poster_30x30"></div>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="poster_p" value="1"> <b>Poster P</b> <div id="reposicao_poster_p"></div>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="poster_g" value="1"> <b>Poster G</b> <div id="reposicao_poster_g"></div>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="filete_20x20" value="1"> <b>Filete 20x20</b> <div id="reposicao_filete_20x20"></div>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="filete_30x30" value="1"> <b>Filete 30x30</b> <div id="reposicao_filete_30x30"></div>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="filete_p" value="1"> <b>Filete P</b> <div id="reposicao_filete_p"></div>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="filete_g" value="1"> <b>Filete G</b> <div id="reposicao_filete_g"></div>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="sobmedida" value="1"> <b>Sob Medida</b> <div id="reposicao_sobmedida"></div>
							</label>
						</div>
						<button type="submit" class="btn btn-default" id="btn_exportar_reposicao" disabled>Exportar</button>
					</form>
				</div>
			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="//malsup.github.com/jquery.form.js"></script>
		<script src="/assets/js/engine_menu.js"></script>
		<script src="/assets/js/engine_relatorio.js"></script>
		<script type="text/javascript">
			<?php if($this->session->flashdata('error_upload')){ ?>
			alert("<?php echo $this->session->flashdata('error_upload'); ?>");
			<?php } ?>
			
			$(document).ready(function(event) {
				$('#form_upload_import_reposicao').ajaxForm(function(data) {
					var arr_response = data.split('|');
					
					$('#reposicao_ima_15x15').html(arr_response[0]);
					
					$('#reposicao_poster_20x20').html(arr_response[1]);
					$('#reposicao_poster_30x30').html(arr_response[2]);
					$('#reposicao_poster_p').html(arr_response[3]);
					$('#reposicao_poster_g').html(arr_response[4]);
					
					$('#reposicao_filete_20x20').html(arr_response[5]);
					$('#reposicao_filete_30x30').html(arr_response[6]);
					$('#reposicao_filete_p').html(arr_response[7]);
					$('#reposicao_filete_g').html(arr_response[8]);
					
					$('#reposicao_sobmedida').html(arr_response[9]);
					
					$('#btn_exportar_reposicao').attr('disabled', false);
				});
			});
		</script>
	</body>
</html>