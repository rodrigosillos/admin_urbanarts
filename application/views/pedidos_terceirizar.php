<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Administrador Urban Arts - Pedidos / Terceirizar</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<?php include_once('includes/menu.php'); ?>
			<h1>Terceirização</h1>
			<div class="row">
				<div class="col-md-12" style="margin-bottom: 20px;">
					<form class="form-inline" name="form_filtro" method="post" action="">
						<div class="form-group">
							<label for="tipo_pedido">Tipo de Pedido</label>
							<select class="form-control" id="tipo_pedido" name="tipo_pedido">
									<option value="">Selecione...</option>
								<?php foreach($pedido_tipo as $tipo_pedido): ?>
									<option value="<?php echo $tipo_pedido->tipo_pedido; ?>" <?php if(isset($_POST['tipo_pedido'])){ if($_POST['tipo_pedido']==$tipo_pedido->tipo_pedido){ echo " selected"; } } ?>><?php echo $tipo_pedido->tipo_pedido; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="loja">Loja</label>
							<select class="form-control" id="loja" name="loja">
								<option value="">Selecione...</option>
								<?php foreach($lojas as $loja): ?>
									<option value="<?php echo $loja->id; ?>" <?php if(isset($_POST['loja'])){ if($_POST['loja']==$loja->id){ echo " selected"; } } ?>><?php echo $loja->nome; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="status">Status</label>
							<select class="form-control" id="status" name="status">
								<option value="">Selecione...</option>
								<?php foreach($pedido_status as $status): ?>
									<option value="<?php echo $status->status; ?>"<?php if(isset($_POST['status'])){ if($_POST['status']==$status->status){ echo " selected"; } } ?>><?php echo $status->status; ?></option>
								<?php endforeach; ?>
							</select>
						</div>						
						<!--<button type="submit" class="btn btn-default">Enviar convite</button>-->
					</form>					
				</div>
				<div class="col-md-8">
					<form name="form_pedidos_terceirizar" method="post" action="/pedidos/exporta_terceirizacao" enctype="multipart/form-data">
						<table class="table table-striped">
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>Pedido</td>
								<td>Tipo</td>
								<td>Status</td>
								<td>Cliente</td>
							</tr>
							<?php foreach($pedidos as $pedido): ?>
							<tr>
								<td>
									<input type="checkbox" name="pedido_id[]" value="<?php echo $pedido->id; ?>">
								</td>
								<td><?php echo $pedido->id; ?></td>
								<td><?php echo $pedido->tipo_pedido; ?></td>
								<td><?php echo $pedido->status; ?></td>
								<td><?php echo $pedido->cliente; ?></td>
							</tr>
							<?php endforeach; ?>
						</table>						
						<button type="submit" class="btn btn-default">Exportar</button>
					</form>
				</div>
			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="/assets/js/engine_menu.js"></script>
		<script src="/assets/js/engine_pedido.js"></script>
	</body>
</html>