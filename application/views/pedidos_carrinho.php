<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Administrador Urban Arts - Pedidos / Carrinho</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<?php include_once('includes/menu.php'); ?>
			<h1>Carregar Carrinho</h1>
			<div class="row">
				<div class="col-md-4">
					<form name="form_relatorio_reposicao" method="post" action="/pedidos/carrega_carrinho" enctype="multipart/form-data">
						<div class="form-group">
							<label for="planilha_excel">Planilha Excel ( Relatório de Reposição )</label>
							<input type="file" id="planilha_excel" name="planilha_excel">
							<p class="help-block">.xls .xlsx</p>
						</div>
						<div class="form-group">
							<label for="loja">Carrinho da Loja</label>
							<select class="form-control" id="loja" name="loja">
								<option value="">Selecione ...</option>
								<?php foreach($lojas as $loja): ?>
									<option value="<?php echo $loja->id; ?>"><?php echo $loja->nome; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<button type="submit" class="btn btn-default">Carregar Carrinho</button>
					</form>
				</div>
			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="/assets/js/engine_menu.js"></script>
		<script type="text/javascript">
		<?php if($this->session->flashdata('error_upload')){ ?>
		alert("<?php echo $this->session->flashdata('error_upload'); ?>");
		<?php } ?>
		</script>
	</body>
</html>