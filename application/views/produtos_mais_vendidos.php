<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Administrador Urban Arts - Produtos / Mais Vendidos</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<?php include_once('includes/menu.php'); ?>
			<h1>Mais Vendidos</h1>
			<div class="row">
				<div class="col-md-4" style="margin-bottom: 20px;">
					<form name="form_filtro" method="post" action="/produtos/exporta_mais_vendidos">
						<div class="form-group">
							<label for="data_inicio">Data Início</label>
							<input type="text" class="form-control" id="data_inicio" name="data_inicio" placeholder="">
						</div>
						<div class="form-group">
							<label for="data_final">Data Final</label>
							<input type="text" class="form-control" id="data_final" name="data_final" placeholder="">
						</div>
						<div class="form-group">
							<label for="quantidade">Quantidade de Produtos</label>
							<input type="text" class="form-control" id="limite" name="limite" placeholder="">
						</div>						
						<button type="submit" class="btn btn-default">Gerar</button>
					</form>
				</div>
			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="/assets/js/jquery.maskedinput.min.js"></script>
		<script type="text/javascript">
			$("#data_inicio").mask("99/99/9999");
			$("#data_final").mask("99/99/9999");
		</script> 		
		<script src="/assets/js/engine_menu.js"></script>
	</body>
</html>