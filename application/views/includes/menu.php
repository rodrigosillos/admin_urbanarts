<?php

$segment1 = $this->uri->segment(1);
$segment2 = $this->uri->segment(2);
$segment3 = $this->uri->segment(3);

if($segment2){
  $aba_selecionada = $segment2;
} else {
  $aba_selecionada = $segment1;
}

$item_selecionado = $segment3;

?>
<div class="row">
	<div class="col-md-6">
		<a href="#"><img src="/assets/img/logo.png" style="margin-top: 10px;"></a>
	</div>
	<div class="col-md-6">
		<?php if(isset($usuario_sessao) and !empty($usuario_sessao)){ ?>
		Olá, <?php echo $usuario_sessao[0]->nome; ?>
		<a href="/logout">( Sair )</a>
		<?php } ?>
	</div>
</div>
<hr size="1">
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<!--<label for="menu">Navegar</label>-->
			<select class="form-control" id="menu" name="menu">
				<option value="">Navegar ...</option>
				<option value="">....................................................................................</option>
				<option value="/relatorios/reposicao" <?php if($aba_selecionada == 'reposicao'){ echo "selected"; } ?>>Relatório / Reposição</option>
				<option value="/relatorios/estoque" <?php if($aba_selecionada == 'estoque'){ echo "selected"; } ?>>Relatório / Estoque</option>
				<option value="/relatorios/pedidos" <?php if($aba_selecionada == 'pedidos'){ echo "selected"; } ?>>Relatório / Pedidos</option>
				<option value="">....................................................................................</option>
				<option value="/pedidos/carrinho" <?php if($aba_selecionada == 'carrinho'){ echo "selected"; } ?>>Pedidos / Carregar Carrinho</option>
				<option value="/pedidos/novo" <?php if($aba_selecionada == 'robo'){ echo "selected"; } ?>>Pedidos / Novo</option>
				<option value="/pedidos/terceirizar" <?php if($aba_selecionada == 'terceirizar'){ echo "selected"; } ?>>Pedidos / Terceirizar</option>
				<option value="">....................................................................................</option>
				<option value="/produtos/mais_vendidos" <?php if($aba_selecionada == 'mais_vendidos'){ echo "selected"; } ?>>Produtos / Mais Vendidos</option>
			</select>
		</div>
	</div>
</div>