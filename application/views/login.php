<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Administrador Urban Arts - Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<?php include_once('includes/menu_login.php'); ?>
			<h2>Administrador</h2>
			<div class="row">
				<div class="col-md-4">
					<form name="form_login" method="post" action="">
						<div class="form-group">
							<label for="email">E-mail</label>
							<input type="text" class="form-control" id="email" name="email" placeholder="">
						</div>
						<div class="form-group">
							<label for="senha">Senha</label>
							<input type="password" class="form-control" id="senha" name="senha" placeholder="">
						</div>
						<button type="submit" class="btn btn-default" id="btn-login">Entrar</button>
					</form>
				</div>
			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="/assets/js/engine_login.js"></script>
		<script type="text/javascript">
		<?php if($this->session->flashdata('error_upload')){ ?>
		alert("<?php echo $this->session->flashdata('error_upload'); ?>");
		<?php } ?>
		</script>
	</body>
</html>