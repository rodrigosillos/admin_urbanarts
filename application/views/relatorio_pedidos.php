

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<?php

			set_time_limit(360000);
		?>
		<title>Administrador Urban Arts - Relatório de Estoque</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="/assets/js/engine_menu.js"></script>
		<script src="/assets/js/engine_relatorio.js"></script>
		<script src="/assets/js/jquery.maskedinput.min.js"></script>
		<script>
			
			$(function(){
			 
			 $('#select_all').click(function(event){
				if(this.checked) {
				  $('.checkbox_pedido_id').each(function(){
					this.checked = true;
				  });
				}else{
				  $('.checkbox_pedido_id').each(function(){
					this.checked = false;
				  });       
				}
			  });  	
		   });
		</script>
	</head>
	<body>		
		<div class="container">							
				<form name="form_relatorio_pedidos" method="post">
					<div class="bs-example">
						<?php include_once('includes/menu.php'); ?>
						<h1>Relatório de Pedidos</h1>
						<div class="row">
							<div class="col-md-3">
								<label>Data do pedido: de</label>
							</div>
							<div class="col-md-3"><input type="text" class="form-control" name="dataInicial" id="dataInicial"></div>
							<div class="col-md-3"><label>até</label></div>
							<div class="col-md-3"><input type="text" class="form-control" name="dataFinal" id=dataFinal></div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="loja">Lojas</label>
								<div class="form-control" style="height: 100px; overflow: auto;">						
									<?php foreach($lojas as $loja): ?>
									<div class="checkbox">								
										<label><input type="checkbox" name="<?php echo $loja->id; ?>" id="<?php echo $loja->id; ?>" value="<?php echo $loja->id; ?>"><?php echo $loja->nome; ?></label>								
									  </div>	
									<?php endforeach; ?>
								</div>	
							</div>
							<div class="col-md-6">
								<label for="loja">Tipos de pedido</label>
								<div class="form-control" style="height: 100px; overflow: auto;">						
									<?php foreach($tipos_pedido as $tipo_pedido): ?>
									<div class="checkbox">								
										<label><input type="checkbox" name="<?php echo $tipo_pedido->descricao; ?>" value="<?php echo $tipo_pedido->descricao; ?>"><?php echo $tipo_pedido->descricao; ?></label>								
									  </div>	
									<?php endforeach; ?>
								</div>							
							</div>
						</div>				
						<div class="row">
							<div class="col-md-12">
								<label for="loja">Tipo de produto</label>
								<div class="checkbox">								
									<label><input type="checkbox" name="PosterFilete" id="PosterFilete">Pôster Filete</label>
									<label><input type="checkbox" name="SobMedida" id="SobMedida">Sob Medida</label>
									<label><input type="checkbox" name="PosterPadrao" id="PosterPadrão">Pôster Padrão</label>									
								</div>							
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<button <?php if( count($pedidos) <= 0) { ?> style="display: none;" <?php } ?> class="btn btn-sucess" id="gerarRelatorioPedidos" onclick="direcionar('EXPORTAR');" >Gerar Excel</button>
								<button class="btn btn-primary" id="buscarRelatorioPedidos" onclick="direcionar('EXIBIR');">Buscar</button>				
							</div>
						</div>
				</div>
					<div class="row">
						<br><br>
					</div>
					
						<div class="row" <?php if( count($pedidos) <= 0) { ?> style="display: none;" <?php } ?>  >
							<div class="col-md-12">						
								<table class="table table-bordered table-striped">
									<th>
										<input type="checkbox" id="select_all">
										<!--<input type="checkbox" name="todosPedidos" id="todosPedidos" onchange="selecionarTodosPedidos(this);">-->
									</th>
									<th>Pedido</th>
									<th>Data do pedido</th>
									<th>Cliente</th>
									<th>Tipo de pedido</th>
									<th>Total</th>
									<th>Status</th>
													
									<?php foreach($pedidos as $pedido): ?>								
										<tr>
											<td>
												<input type="checkbox" name="pedido_id[]" class="checkbox_pedido_id" value="<?php echo $pedido->pedido; ?>">											
											</td>
											<td><?php echo $pedido->pedido; ?></td>
											<td><?php echo date("d/m/Y", strtotime($pedido->data_pedido)); ?></td>
											<td><?php echo $pedido->cliente; ?></td>
											<td><?php echo $pedido->tipo_pedido; ?></td>
											<td><?php echo money_format('%n', $pedido->total); ?></td>
											<td><?php echo $pedido->status_pedido; ?></td>
										</tr>													
									<?php endforeach; ?>							
								</table>
							</div>
						</div>				
				</form>
		</div>
			
		<script type="text/javascript">
			$("#dataInicial").mask("99/99/9999");
			$("#dataFinal").mask("99/99/9999");
			
			function direcionar(tipo)
			{				
				if(tipo == "EXPORTAR")
				{
					var temSelecionado = false;
					$('.checkbox_pedido_id').each(function(){
						
						if(this.checked)
						temSelecionado = true;
					});
					if(temSelecionado)
					{
						document.form_relatorio_pedidos.action = '/relatorios/gerar_excel';
						document.form_relatorio_pedidos.submit();
					}
					else
					{
						alert('Selecione os pedidos que deseja exportar para Excel.');
						return;
					}
					//location.href = "/relatorios/gerar_excel";					
				}
				else
				{
					document.form_relatorio_pedidos.action = '/relatorios/gerar_relatorio_pedido';
					document.form_relatorio_pedidos.submit();
					//$("#form_relatorio_pedidos").attr("action","/relatorios/gerar_relatorio_pedido");
					//$("#form_relatorio_pedidos").attr("action","/relatorios/gerar_relatorio_pedido");					
				}
			}
								
			function selecionarTodosPedidos(obj)
			{
				var item = obj.checked;
				<?php foreach($pedidos as $pedido): ?>
					if(item)
					{
						var chk = document.getElementById(<?php echo $pedido->pedido; ?>);
						chk.checked = true;
						//$("#"+ <?php echo $pedido->pedido; ?>).attr('checked', true);					
					}
					else
					{
						var chk = document.getElementById(<?php echo $pedido->pedido; ?>);
						chk.checked = false;
						//$("#"+ <?php echo $pedido->pedido; ?>).attr('checked', false);
					}
				<?php endforeach; ?>
			}
			
		</script>
	</body>
</html>