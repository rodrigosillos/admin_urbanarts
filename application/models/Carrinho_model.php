<?php

class Carrinho_Model extends CI_Model {
	
	public $table = "carrinho_novo";
	public $table_item = "carrinho_item_novo";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get($data)
	{
		$this->db2->select(array('id'));
		$this->db2->from($this->table);
		$this->db2->where($data);
		
		$query = $this->db2->get();
		return $query->result();
	}	
	
	function set($data, $table=false)
	{
		if(!$table)
		{
			$table = $this->table;
		}
		
		$this->db2->insert($table, $data);
		$insert_id = $this->db2->insert_id();
		return $insert_id;
	}
}