<?php

class Lojas_Model extends CI_Model {
	
	public $table_loja = "clientes_cliente";
	public $table_produto = "produtos_produto";
	public $table_lojista = "lojistas";
	
	function __construct()
	{
		parent::__construct();
	}
	
	
	function get_lojistas()
	{
		$this->db2->select(array('id','nome'));
		$this->db2->from($this->table_lojista);
		$this->db2->order_by('nome', 'desc');
		
		$query = $this->db2->get();
		return $query->result();
	}
	
	function get()
	{
		$this->db2->select(array('id', 'nome'));
		$this->db2->from($this->table_loja);
		$this->db2->where(array('lojista' => 1, 'ativo' => 1));
		$this->db2->order_by('nome', 'desc');
		
		$query = $this->db2->get();
		return $query->result();
	}
	
	function get_artista($data)
	{
		$this->db2->select(array('id', 'nome'));
		$this->db2->from($this->table_loja);
		$this->db2->where($data);
		
		$query = $this->db2->get();
		return $query->result();
	}
	
	function get_artista_by_produto($data)
	{
		$this->db2->select(array('artista_id'));
		$this->db2->from($this->table_produto);
		$this->db2->where($data);
		
		$query = $this->db2->get();
		return $query->result();
	}	
	
	function get_by($where=false)
	{
		$this->db2->select(array('id', 'nome', 'cpf', 'email', 'cep', 'endereco', 'numero', 'complemento', 'referencia', 'bairro', 'cidade', 'telefone', 'estado'));
		$this->db2->from($this->table_loja);
		
		if($where)
		{
			$this->db2->where($where);	
		}		
		
		$query = $this->db2->get();
		return $query->result();
	}
}