<?php

class Relatorios_Model extends CI_Model {
	
	public $table_005 = "millennium_005_ranking_produtos";
	public $table_012 = "millennium_012_estoque_disponivel";	
	
	function __construct()
	{
		parent::__construct();
	}
	
	
	function get($data, $param)
	{
		$this->db->from($param);
		$this->db->where($data);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	
	function set($data, $param)
	{
		$this->db->insert($param, $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	function truncate($param)
	{
		$this->db->truncate($param);
	}
	
	function m_005_ranking_produtos($where, $acabamento, $tamanho)
	{
		$this->db->from($this->table_005);
		$this->db->where($where);
		$this->db->where_in('acabamento', $acabamento);
		
		if($tamanho)
		{
			$this->db->where_in('tamanho', $tamanho);	
		}
		
		$this->db->order_by("dias_estoque");
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	
	function m_005_ranking_produtos_sobmedida($where, $acabamento, $tamanho)
	{
		$this->db->from($this->table_005);
		$this->db->where($where);
		$this->db->where_in('acabamento', $acabamento);
		
		if($tamanho)
		{
			$this->db->where_not_in('tamanho', $tamanho);	
		}
		
		$this->db->order_by("dias_estoque");
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}	
	
	function m_012_estoque_disponivel($where=false, $acabamento, $tamanho)
	{
		$this->db->from($this->table_012);
		
		if($where)
		{
			$this->db->where($where);
		}
		
		$this->db->where_in('acabamento', $acabamento);
		
		if($tamanho){
			$this->db->where_in('tamanho', $tamanho);	
		}
		
		$this->db->order_by("estoque");
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
}