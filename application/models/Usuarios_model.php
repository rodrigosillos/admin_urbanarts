<?php

class Usuarios_Model extends CI_Model {
	
	public $table = "usuarios";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function auth($data)
	{
		$this->db->select(array('nome', 'email'));
		$this->db->from($this->table);
		$this->db->where($data);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
}