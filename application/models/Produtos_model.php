<?php

class Produtos_Model extends CI_Model {
	
	public $table = 'produtos_produto';
	public $table_tamanho = 'produtos_tamanho_novo';
	public $table_trabalho = 'trabalhos_trabalho';
	public $table_preco = 'produtos_preco';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get_utf8($data)
	{
		$this->db2->select(array('id', 'titulo_ptbr', 'artista_id', 'formato_id'));
		$this->db2->from($this->table);
		$this->db2->where("titulo_ptbr = _utf8 '" . $data . "' collate utf8_unicode_ci");
		
		$query = $this->db2->get();
		return $query->result();
	}
	
	function get_preco($data, $acabamento, $tamanho)
	{
		$this->db2->select(array('valor', 'custo', 'custo_reposicao'));
		$this->db2->from($this->table_preco);
		$this->db2->where($data);
		$this->db2->where_in('acabamento_id', $acabamento);
		$this->db2->where_in('tamanho_id', $tamanho);
		$this->db2->limit(1);
		
		$query = $this->db2->get();
		//echo $this->db2->last_query();
		return $query->result();
	}
	
	function get($select=false, $table=false, $where=false)
	{
		if($select)
		{
			$this->db2->select($select);
		}
		
		if(!$table)
		{
			$table = $this->table;
		}
		
		$this->db2->from($table);
		
		if($where)
		{
			$this->db2->where($where);	
		}		
		
		$query = $this->db2->get();
		return $query->result();
	}	
	
	function set($data, $table=false)
	{
		if(!$table)
		{
			$table = $this->table;
		}
		
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	function update($produto_id, $data)
	{
		$this->db2->where('id', $produto_id);
		$this->db2->limit(1);
		$this->db2->update($this->table, $data);
	}
	
}