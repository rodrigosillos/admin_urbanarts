<?php

class Pedidos_Model extends CI_Model {
	
	public $table_produto = "produtos_produto";
	public $table_pedido = "pedidos_pedido";
	public $table_pedido_item = "pedidos_pedidoitem";
	public $table_tipo_pedido = "tipo_pedido";
	public $table_relatorio_pedidos = "relatorio_pedidos";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get_relatorio_pedidos($dataInicial=false, $dataFinal=false, $lojas=false, $tipos_pedido=false, $tipos_produto=false)
	{		
		$this->db2->select(array('pedido', 'data_pedido', 'cliente', 'tipo_pedido','status_pedido'));
		$this->db2->select_sum('preco_final','total');
		//$this->db2->select('COUNT(DISTINCT(pedido.cliente_id)) quantidade_lojas');
		$this->db2->from($this->table_relatorio_pedidos);						
		//$this->db2->where($data);
		if($dataInicial)
		{
			$this->db2->where('data_pedido >=', $dataInicial);
		}
		if($dataFinal)
		{
			$this->db2->where('data_pedido <=', $dataFinal);
		}
		if($lojas)
			$this->db2->where_in('cliente_id', $lojas);
			
		if($tipos_pedido)
			$this->db2->where_in('tipo_pedido', $tipos_pedido);
			
		if($tipos_produto)
			$this->db2->where_in('apelido_id', $tipos_produto);
			
		$this->db2->group_by(array('pedido', 'data_pedido', 'cliente', 'tipo_pedido','status_pedido'));
		$this->db2->order_by('pedido', 'desc');
		$query = $this->db2->get();
		//echo $this->db2->last_query();
		return $query->result();
	}
	function get_relatorio_pedidos_excel_com_filtro($dataInicial=false, $dataFinal=false, $lojas=false, $tipos_pedido=false, $tipos_produto=false)
	{
		$this->db2->select(array('data_pedido','pedido','produto','artista','descricao_pedido','quantidade','cliente','preco_unitario','preco_final','status_pedido','valor_custo','valor_artista','total_custo','total_custo_producao','prazo_envio','tipo_pedido','acabamento', 'tamanho','apelido_produto','formato' )); 
		$this->db2->from($this->table_relatorio_pedidos);
		
		if($dataInicial)
		{
			$this->db2->where('data_pedido >=', $dataInicial);
		}
		if($dataFinal)
		{
			$this->db2->where('data_pedido <=', $dataFinal);
		}
		if($lojas)
			$this->db2->where_in('cliente_id', $lojas);
			
		if($tipos_pedido)
			$this->db2->where_in('tipo_pedido', $tipos_pedido);
			
		if($tipos_produto)
			$this->db2->where_in('apelido_id', $tipos_produto);
			
		$this->db2->order_by('pedido', 'desc');			
		$query = $this->db2->get();
		
		return $query->result();
	}
	
	function get_relatorio_pedidos_excel($data)
	{
		$this->db2->select(array('data_pedido','pedido','produto','artista','descricao_pedido','quantidade','cliente','preco_unitario','preco_final','status_pedido','valor_custo','valor_artista','total_custo','total_custo_producao','prazo_envio','tipo_pedido','acabamento', 'tamanho','apelido_produto','formato' )); 
		$this->db2->from($this->table_relatorio_pedidos);
		
		if($data){
			$this->db2->where_in('pedido', $data);
		  }
		$this->db2->order_by('pedido', 'desc');			
		$query = $this->db2->get();
		
		return $query->result();
	}
	function get_tipos_pedido()
	{
		$this->db2->select(array('descricao'));
		$this->db2->from($this->table_tipo_pedido);
		$this->db2->where_in('descricao', array('TROCA', 'REPOSIÇÃO', 'ENCOMENDA','NATAL','MIX','MOLDURA'));
		$this->db2->group_by(array('descricao'));
		$this->db2->order_by('descricao');
		
		$query = $this->db2->get();
		return $query->result();
	}
	
	function get($select=false, $table=false, $where=false, $limit=false)
	{
		if($select)
		{
			$this->db2->select($select);
		}
		
		if($table==false)
		{
			$table = $this->table_pedido;
			
		}
		
		$this->db2->from($table);
		
		if($where)
		{
			$this->db2->where($where);
		}
		
		$this->db2->order_by('id', 'desc');
		
		if($limit)
		{
			$this->db2->limit($limit);
		}
		
		$query = $this->db2->get();
		//echo $this->db2->last_query();
		return $query->result();
	}
	
	function get_group_by($select=false, $table=false, $where=false, $group_by=false)
	{
		$this->db2->select($select);
		$this->db2->from($table);
		$this->db2->where($where);
		$this->db2->group_by($group_by);
		
		$query = $this->db2->get();
		//echo $this->db2->last_query();
		return $query->result();
	}
	
	function get_item($select=false, $where=false, $where_acabamento=false, $where_tamanho)
	{
		if($select)
		{
			$this->db2->select($select);
		}
		
		$this->db2->from($this->table_pedido_item);
		
		if($where)
		{
			$this->db2->where($where);
		}		
		
		if($where_acabamento)
		{
			$this->db2->where_in('acabamento_id', $where_acabamento);
		}
		
		if($where_tamanho)
		{
			$this->db2->where_in('tamanho_id', $where_tamanho);
		}
		
		$query = $this->db2->get();
		return $query->result();
	}	
	
	function set($data, $table)
	{
		$this->db2->insert($table, $data);
		$insert_id = $this->db2->insert_id();
		return $insert_id;
	}
	
	function update($pedido_id, $data)
	{
		$this->db2->where('id', $pedido_id);
		$this->db2->limit(1);
		$this->db2->update($this->table_pedido, $data);
	}
	
	function produtos_mais_vendidos($data_inicio=false, $data_final=false, $limite=200)
	{
		$this->db2->select(array('produto.id', 'produto.titulo_ptbr nome', 'produto.data data_cadastro'));
		$this->db2->select_sum('item.quantidade');
		//$this->db2->select('COUNT(DISTINCT(pedido.cliente_id)) quantidade_lojas');
		$this->db2->from($this->table_produto.' produto');
		$this->db2->join('pedidos_pedidoitem item', 'produto.id = item.produto_id');
		$this->db2->join('pedidos_pedido pedido', 'pedido.id = item.pedido_id');
		$this->db2->where(array('produto.tipo_id' => 1, 'produto.ativo' => 1));
		$this->db2->where('pedido.data >=', $data_inicio);
		$this->db2->where('pedido.data <=', $data_final);
		$this->db2->where_not_in('pedido.status', array('CANCELADO', 'AGUARDANDO PAGAMENTO', 'PENDENTE'));
		$this->db2->group_by(array('item.produto_id'));
		$this->db2->order_by('quantidade', 'desc');
		$this->db2->limit($limite);
		
		$query = $this->db2->get();
		//echo $this->db2->last_query();
		return $query->result();
	}	
	
	function desmembra_venda($data_inicio=false, $data_final=false, $produto=false)
	{
		$this->db2->select(array('acabamento.titulo_ptbr acabamento', 'tamanho.titulo_ptbr tamanho'));
		$this->db2->select_sum('item.quantidade');
		$this->db2->from($this->table_pedido_item.' item');
		$this->db2->join('pedidos_pedido pedido', 'pedido.id = item.pedido_id');
		$this->db2->join('produtos_acabamento acabamento', 'acabamento.id = item.acabamento_id');
		$this->db2->join('produtos_tamanho_novo tamanho', 'tamanho.id = item.tamanho_id');
		$this->db2->where_not_in('pedido.status', array('CANCELADO', 'AGUARDANDO PAGAMENTO', 'PENDENTE'));
		$this->db2->where(array('item.produto_id' => $produto));
		$this->db2->where('pedido.data >=', $data_inicio);
		$this->db2->where('pedido.data <=', $data_final);
		$this->db2->group_by(array('item.acabamento_id', 'item.tamanho_id'));
		$this->db2->order_by('quantidade', 'desc');
		
		$query = $this->db2->get();
		//echo $this->db2->last_query();
		return $query->result();
	}
	
	function contagem_lojas($data_inicio=false, $data_final=false, $produto=false)
	{
		$this->db2->select('COUNT(DISTINCT(pedido.cliente_id)) quantidade_lojas');
		$this->db2->from($this->table_pedido_item.' item');
		$this->db2->join('pedidos_pedido pedido', 'pedido.id = item.pedido_id');
		$this->db2->join('clientes_cliente cliente', 'cliente.id = pedido.cliente_id');
		$this->db2->where_not_in('pedido.status', array('CANCELADO', 'AGUARDANDO PAGAMENTO', 'PENDENTE'));
		$this->db2->where(array('item.produto_id' => $produto));
		$this->db2->where('pedido.data >=', $data_inicio);
		$this->db2->where('pedido.data <=', $data_final);
		$this->db2->where(array('cliente.lojista' => 1));
		
		$query = $this->db2->get();
		//echo $this->db2->last_query();
		return $query->result();		
	}
}