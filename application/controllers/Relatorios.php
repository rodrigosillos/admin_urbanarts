<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('lojas_model','relatorios_model', 'pedidos_model'));
    }

    function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fprintf($df, chr(0xEF).chr(0xBB).chr(0xBF));
        fputcsv($df, array_keys(reset($array)), ';');
        foreach ($array as $row) {
            fputcsv($df, $row, ';');
        }
        fclose($df);
        return ob_get_clean();
    }

    function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    public function reposicao()
    {
        $data = array('usuario_sessao' => $this->usuario_sessao);
        $this->load->view('relatorios_reposicao', $data);
    }

    public function estoque()
    {
        $data = array('usuario_sessao' => $this->usuario_sessao);
        $this->load->view('relatorios_estoque', $data);
    }

    public function pedidos()
    {
        $lojas = $this->lojas_model->get_lojistas();
        $tipos_pedido = $this->pedidos_model->get_tipos_pedido();
        $pedidos = array();
        $data = array('lojas' => $lojas,
            'usuario_sessao' => $this->usuario_sessao,
            'tipos_pedido' => $tipos_pedido,
            'pedidos' => $pedidos);
        $this->load->view('relatorio_pedidos', $data);
    }

    public function gerar_relatorio_pedido()
    {
        //filtro de datas
        $arr_data_inicio = explode('/', $_POST['dataInicial']);
        if(count($arr_data_inicio) == 3)
        {$dataInicial = $arr_data_inicio[2] .'-'. $arr_data_inicio[1] .'-'. $arr_data_inicio[0] . ' 00:00:00';}
        else{
            $dataInicial = '';
        }
        $arr_data_Final = explode('/', $_POST['dataFinal']);
        if(count($arr_data_Final) == 3)
        {	$dataFinal = $arr_data_Final[2] .'-'. $arr_data_Final[1] .'-'. $arr_data_Final[0] . ' 23:59:59';}
        else{$dataFinal = '';}

        $lojas = $this->lojas_model->get_lojistas();
        $tipos_pedido = $this->pedidos_model->get_tipos_pedido();

        $param = array();
        $data_lojas = array();
        $data_tipos_pedido = array();
        $data_tipos_produto = array();
        if(isset($_POST['PosterFilete']) )
        {
            array_push($data_tipos_produto, 2);
        }
        if(isset($_POST['PosterPadrao']) )
        {
            array_push($data_tipos_produto, 0);
        }
        if(isset($_POST['SobMedida']) )
        {
            array_push($data_tipos_produto,1);
        }

        foreach($lojas as $loja)
        {
            $lojaSelecionada = isset($_POST[$loja->id]) ? true : false;
            if($lojaSelecionada)
            {
                array_push($data_lojas, $loja->id);
            }
        }

        foreach($tipos_pedido as $tipo_pedido)
        {
            $tipoPedidoSelecionado = isset($_POST[$tipo_pedido->descricao]) ? true : false;
            if($tipoPedidoSelecionado)
            {
                array_push($data_tipos_pedido, $tipo_pedido->descricao);
            }
        }

        array_unique($data_lojas);
        array_unique($data_tipos_pedido);
        //$result = $this->relatorios_model->m_012_estoque_disponivel(false, $data_acabamento, $data_tamanho);//filtro de lojas

        //$pedidos = $this->pedidos_model->get_relatorio_pedidos($dataInicial, $dataFinal, $data_lojas, $data_tipos_pedido, $data_tipos_produto);

        //$data = array('lojas' => $lojas,
        //			  'usuario_sessao' => $this->usuario_sessao,
        //			  'tipos_pedido' => $tipos_pedido,
        //			  'pedidos' => $pedidos);

        //$this->load->view('relatorio_pedidos', $data);

        $itens = $this->pedidos_model->get_relatorio_pedidos_excel_com_filtro($dataInicial, $dataFinal, $data_lojas, $data_tipos_pedido, $data_tipos_produto);

        foreach ($itens as $item) {
            $item->prazo_envio = !empty($item->prazo_envio) ? date("d/m/Y", strtotime($item->prazo_envio)) : "";
            $item->data_pedido = !empty($item->data_pedido) ? date("d/m/Y", strtotime($item->data_pedido)) : "";
        }

        $array = json_decode(json_encode($itens), True);
        $this->download_send_headers("data_export_" . date("Y-m-d") . ".csv");
        echo $this->array2csv($array);
        die();
    }

    public function gerar_excel()
    {

        if($_POST){

            $cbx_pedidos = $_POST['pedido_id'];

            //$implode = implode("_", $cbx_pedidos);

            $itens = $this->pedidos_model->get_relatorio_pedidos_excel($cbx_pedidos);

            $this->load->library('PHPExcel');

            $sheet = new PHPExcel();

            $sheet->getProperties()->setTitle('Relatório de Pedidos')->setDescription('Relatório de Pedidos');

            $sheet->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Data do Pedido')
                ->setCellValue('B1', 'Pedido')
                ->setCellValue('C1', 'Produto')
                ->setCellValue('D1', 'Artista')
                ->setCellValue('E1', 'Descrição do Pedido')
                ->setCellValue('F1', 'Quantidade')
                ->setCellValue('G1', 'Cliente')
                ->setCellValue('H1', 'Preço unitário')
                ->setCellValue('I1', 'Preço Final')
                ->setCellValue('J1', 'Status do Pedido')
                ->setCellValue('K1', 'Valor Custo')
                ->setCellValue('L1', 'Valor Artista')
                ->setCellValue('M1', 'Total Custo')
                ->setCellValue('N1', 'Total Custo Produção')
                ->setCellValue('O1', 'Prazo Envio')
                ->setCellValue('P1', 'Tipo de Pedido')
                ->setCellValue('Q1', 'Acabamento')
                ->setCellValue('R1', 'Tamanho')
                ->setCellValue('S1', 'Tipo Geral de Pedido')
                ->setCellValue('T1', 'Formato');

            $p = 2;

            foreach($itens as $item){

                $prazo_envio = !empty($item->prazo_envio) ? date("d/m/Y", strtotime($item->prazo_envio)) : "";

                if($prazo_envio != "")
                {
                    $ano_prazo_envio = date('Y', strtotime($item->prazo_envio));
                    $mes_prazo_envio = date('m', strtotime($item->prazo_envio));
                    $dia_prazo_envio = date('d', strtotime($item->prazo_envio));
                    $valor_prazo_envio = '=DATE('.$ano_prazo_envio.','.$mes_prazo_envio.','.$dia_prazo_envio.')';
                }
                else
                {
                    $valor_prazo_envio = "";
                }

                $ano_pedido = date('Y', strtotime($item->data_pedido));
                $mes_pedido = date('m', strtotime($item->data_pedido));
                $dia_pedido = date('d', strtotime($item->data_pedido));
                $valor = '=DATE('.$ano_pedido.','.$mes_pedido.','.$dia_pedido.')';
                $sheet->setActiveSheetIndex(0)
                    //->setCellValue('A'.$p, '=DATA('.$ano_pedido.';'.$mes_pedido.';'.$dia_pedido.')')
                    ->setCellValue('A'.$p, $valor)
                    //->setCellValue('A'.$p, (($item->data_pedido)))
//				->setCellValue('A'.$p, date("d/m/Y", strtotime($item->data_pedido)))
                    //->getStyle('A'.$p)
                    //->getNumberFormat()
                    //->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2)
                    //->setCellValue('A'.$p, date("d/m/Y", strtotime($item->data_pedido)))
                    ->setCellValue('B'.$p, $item->pedido)
                    ->setCellValue('C'.$p, $item->produto)

                    ->setCellValue('D'.$p, $item->artista)
                    ->setCellValue('E'.$p, $item->descricao_pedido)
                    ->setCellValue('F'.$p, $item->quantidade)
                    ->setCellValue('G'.$p, $item->cliente)
                    ->setCellValue('H'.$p, $item->preco_unitario)
                    ->setCellValue('I'.$p, $item->preco_final)
                    ->setCellValue('J'.$p, $item->status_pedido)
                    ->setCellValue('K'.$p, $item->valor_custo)
                    ->setCellValue('L'.$p, $item->valor_artista)
                    ->setCellValue('M'.$p, $item->total_custo)
                    ->setCellValue('N'.$p, $item->total_custo_producao)
                    ->setCellValue('O'.$p, $valor_prazo_envio)
                    ->setCellValue('P'.$p, $item->tipo_pedido)
                    ->setCellValue('Q'.$p, $item->acabamento)
                    ->setCellValue('R'.$p, $item->tamanho)
                    ->setCellValue('S'.$p, $item->apelido_produto)
                    ->setCellValue('T'.$p, $item->formato);

                $sheet->getActiveSheet()        // Format as date and time
                ->getStyle('A'.$p)
                    ->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY  );

                if($valor_prazo_envio != "")
                {
                    $sheet->getActiveSheet()        // Format as date and time
                    ->getStyle('O'.$p)
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY  );
                }
                $p++;

            }

            /*header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="relatorio_pedidos.xls"');
            header('Cache-Control: max-age=0');

            header('Cache-Control: max-age=1');

            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0*/

            $objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
            $objWriter->save('/var/www/html/admin_urbanarts/upload/relatorio_pedidos.xls');
            exit;

        }
    }

    public function gerar_excel_por_pedidos()
    {

        if($_POST){

            $cbx_pedidos = $_POST['pedido_id'];

            //$implode = implode("_", $cbx_pedidos);

            $itens = $this->pedidos_model->get_relatorio_pedidos_excel($cbx_pedidos);

            $this->load->library('PHPExcel');

            $sheet = new PHPExcel();

            $sheet->getProperties()->setTitle('Relatório de Pedidos')->setDescription('Relatório de Pedidos');

            $sheet->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Data do Pedido')
                ->setCellValue('B1', 'Pedido')
                ->setCellValue('C1', 'Produto')
                ->setCellValue('D1', 'Artista')
                ->setCellValue('E1', 'Descrição do Pedido')
                ->setCellValue('F1', 'Quantidade')
                ->setCellValue('G1', 'Cliente')
                ->setCellValue('H1', 'Preço unitário')
                ->setCellValue('I1', 'Preço Final')
                ->setCellValue('J1', 'Status do Pedido')
                ->setCellValue('K1', 'Valor Custo')
                ->setCellValue('L1', 'Valor Artista')
                ->setCellValue('M1', 'Total Custo')
                ->setCellValue('N1', 'Total Custo Produção')
                ->setCellValue('O1', 'Prazo Envio')
                ->setCellValue('P1', 'Tipo de Pedido')
                ->setCellValue('Q1', 'Acabamento')
                ->setCellValue('R1', 'Tamanho')
                ->setCellValue('S1', 'Tipo Geral de Pedido')
                ->setCellValue('T1', 'Formato');

            $p = 2;

            foreach($itens as $item){

                $prazo_envio = !empty($item->prazo_envio) ? date("d/m/Y", strtotime($item->prazo_envio)) : "";

                if($prazo_envio != "")
                {
                    $ano_prazo_envio = date('Y', strtotime($item->prazo_envio));
                    $mes_prazo_envio = date('m', strtotime($item->prazo_envio));
                    $dia_prazo_envio = date('d', strtotime($item->prazo_envio));
                    $valor_prazo_envio = '=DATE('.$ano_prazo_envio.','.$mes_prazo_envio.','.$dia_prazo_envio.')';
                }
                else
                {
                    $valor_prazo_envio = "";
                }

                $ano_pedido = date('Y', strtotime($item->data_pedido));
                $mes_pedido = date('m', strtotime($item->data_pedido));
                $dia_pedido = date('d', strtotime($item->data_pedido));
                $valor = '=DATE('.$ano_pedido.','.$mes_pedido.','.$dia_pedido.')';
                $sheet->setActiveSheetIndex(0)
                    //->setCellValue('A'.$p, '=DATA('.$ano_pedido.';'.$mes_pedido.';'.$dia_pedido.')')
                    ->setCellValue('A'.$p, $valor)
                    //->setCellValue('A'.$p, (($item->data_pedido)))
//				->setCellValue('A'.$p, date("d/m/Y", strtotime($item->data_pedido)))
                    //->getStyle('A'.$p)
                    //->getNumberFormat()
                    //->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2)
                    //->setCellValue('A'.$p, date("d/m/Y", strtotime($item->data_pedido)))
                    ->setCellValue('B'.$p, $item->pedido)
                    ->setCellValue('C'.$p, $item->produto)

                    ->setCellValue('D'.$p, $item->artista)
                    ->setCellValue('E'.$p, $item->descricao_pedido)
                    ->setCellValue('F'.$p, $item->quantidade)
                    ->setCellValue('G'.$p, $item->cliente)
                    ->setCellValue('H'.$p, $item->preco_unitario)
                    ->setCellValue('I'.$p, $item->preco_final)
                    ->setCellValue('J'.$p, $item->status_pedido)
                    ->setCellValue('K'.$p, $item->valor_custo)
                    ->setCellValue('L'.$p, $item->valor_artista)
                    ->setCellValue('M'.$p, $item->total_custo)
                    ->setCellValue('N'.$p, $item->total_custo_producao)
                    ->setCellValue('O'.$p, $valor_prazo_envio)
                    ->setCellValue('P'.$p, $item->tipo_pedido)
                    ->setCellValue('Q'.$p, $item->acabamento)
                    ->setCellValue('R'.$p, $item->tamanho)
                    ->setCellValue('S'.$p, $item->apelido_produto)
                    ->setCellValue('T'.$p, $item->formato);

                $sheet->getActiveSheet()        // Format as date and time
                ->getStyle('A'.$p)
                    ->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY  );

                if($valor_prazo_envio != "")
                {
                    $sheet->getActiveSheet()        // Format as date and time
                    ->getStyle('O'.$p)
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY  );
                }
                $p++;

            }

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="relatorio_pedidos.xls"');
            header('Cache-Control: max-age=0');

            header('Cache-Control: max-age=1');

            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
            $objWriter->save('php://output');
            exit;

        }
    }
    public function gerar_relatorio_pedido_excel()
    {
        //filtro de datas

        $arr_data_inicio = explode('/', $_POST['dataInicial']);
        if(count($arr_data_inicio) == 3)
        {$dataInicial = $arr_data_inicio[2] .'-'. $arr_data_inicio[1] .'-'. $arr_data_inicio[0] . ' 00:00:00';}
        else{
            $dataInicial = '';
        }
        $arr_data_Final = explode('/', $_POST['dataFinal']);
        if(count($arr_data_Final) == 3)
        {	$dataFinal = $arr_data_Final[2] .'-'. $arr_data_Final[1] .'-'. $arr_data_Final[0] . ' 23:59:59';}
        else{$dataFinal = '';}

        $lojas = $this->lojas_model->get_lojistas();
        $tipos_pedido = $this->pedidos_model->get_tipos_pedido();

        $param = array();
        $data_lojas = array();
        $data_tipos_pedido = array();
        $data_tipos_produto = array();
        if(isset($_POST['Pôster filete']) )
        {
            array_push($data_tipos_produto, 'Poster filete');
        }
        if(isset($_POST['Pôster Padrão']) )
        {
            array_push($data_tipos_produto, 'Poster Padrão');
        }
        if(isset($_POST['Sob Medida']) )
        {
            array_push($data_tipos_produto, 'Sob Medida');
        }

        foreach($lojas as $loja)
        {
            $lojaSelecionada = isset($_POST[$loja->id]) ? true : false;
            if($lojaSelecionada)
            {
                array_push($data_lojas, $loja->id);
            }
        }

        foreach($tipos_pedido as $tipo_pedido)
        {
            $tipoPedidoSelecionado = isset($_POST[$tipo_pedido->descricao]) ? true : false;
            if($tipoPedidoSelecionado)
            {
                array_push($data_tipos_pedido, $tipo_pedido->descricao);
            }
        }

        array_unique($data_lojas);
        array_unique($data_tipos_pedido);
        //$result = $this->relatorios_model->m_012_estoque_disponivel(false, $data_acabamento, $data_tamanho);//filtro de lojas

        $pedidos = $this->pedidos_model->get_relatorio_pedidos($dataInicial, $dataFinal, $data_lojas, $data_tipos_pedido, $data_tipos_produto);

        $data = array('lojas' => $lojas,
            'usuario_sessao' => $this->usuario_sessao,
            'tipos_pedido' => $tipos_pedido,
            'pedidos' => $pedidos);

        $this->load->view('relatorio_pedidos', $data);
    }

    public function upload_import_reposicao()
    {
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite'] = TRUE;
        $config['remove_spaces'] = FALSE;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('planilha_excel'))
        {
            $this->session->set_flashdata('error_upload', strip_tags($this->upload->display_errors()));
            redirect('/relatorios/reposicao');
        }
        else
        {
            // limpa a tabela antes de importar
            $this->relatorios_model->truncate('millennium_005_ranking_produtos');

            // importa a planilha
            $inputFileName = $_FILES['planilha_excel']['name'];
            $inputFileType = 'Excel2007';
            $upload_path = './upload/';

            $this->load->library('PHPExcel');
            $inputFileType = PHPExcel_IOFactory::identify($upload_path . $inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);

            $arr_data = array();

            $objPHPExcel        = $objReader->load($upload_path . $inputFileName);
            $total_sheets       = $objPHPExcel->getSheetCount(); // here 4
            $allSheetName       = $objPHPExcel->getSheetNames(); // array ([0]=>'student',[1]=>'teacher',[2]=>'school',[3]=>'college')
            $objWorksheet       = $objPHPExcel->setActiveSheetIndex(0);
            $highestRow         = $objWorksheet->getHighestRow(); // here 5
            $highestColumn      = $objWorksheet->getHighestColumn(); // here 'E'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  // here 5

            for ($row = 1; $row <= $highestRow; ++$row)
            {
                for ($col = 0; $col <= $highestColumnIndex; ++$col)
                {
                    $value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();

                    if(is_array($arr_data))
                    {
                        $arr_data[$row-1][$col]=$value;
                    }
                }
            }

            $linha = 1;

            foreach($arr_data as $data)
            {
                if($linha>1)
                {
                    $codigo = $data[0];
                    $produto = $data[1];
                    $tamanho = $data[2];
                    $acabamento = $data[3];
                    $moldura = $data[4];
                    $tipo_pedido = $data[5];
                    $data_venda = $data[6];
                    $quantidade = $data[7];
                    $ultima_entrada = $data[8];
                    $ultima_saida = $data[9];
                    $estoque = $data[10];
                    $dias_estoque = $data[11];

                    $data_venda = date('Y-m-d H:i:s', strtotime($data_venda));
                    $ultima_entrada = date('Y-m-d H:i:s', strtotime($ultima_entrada));
                    $ultima_saida = date('Y-m-d H:i:s', strtotime($ultima_saida));

                    $data = array('codigo' => $codigo,
                        'produto' => $produto,
                        'tamanho' => $tamanho,
                        'acabamento' => $acabamento,
                        'moldura' => $moldura,
                        'tipo_pedido' => $tipo_pedido,
                        'data_venda' => $data_venda,
                        'quantidade' => $quantidade,
                        'ultima_entrada' => $ultima_entrada,
                        'ultima_saida' => $ultima_saida,
                        'estoque' => $estoque,
                        'dias_estoque' => $dias_estoque);

                    $this->relatorios_model->set($data, 'millennium_005_ranking_produtos');
                }
                $linha++;
            }

            //carrega as quantidades
            $param = array('tipo_pedido' => 'PRONTA-ENTREGA');
            $data_acabamento = array('IMA', 'POSTER FILETE', 'PAPEL MATTE', 'CANVAS');
            $data_tamanho = array('15 X 15 CM',
                '20 X 20 CM',
                '30 X 30 CM',
                '36 X 47,5 CM',
                '47,5 X 36 CM',
                '47,5 X 62,5 CM',
                '62,5 X 47,5 CM');

            $result = $this->relatorios_model->m_005_ranking_produtos($param, $data_acabamento, $data_tamanho);

            $quantidade_vendida_ima_15x15 = 0;

            $quantidade_vendida_poster_20x20 = 0;
            $quantidade_vendida_poster_30x30 = 0;
            $quantidade_vendida_poster_p = 0;
            $quantidade_vendida_poster_g = 0;

            $quantidade_vendida_filete_20x20 = 0;
            $quantidade_vendida_filete_30x30 = 0;
            $quantidade_vendida_filete_p = 0;
            $quantidade_vendida_filete_g = 0;

            $quantidade_vendida_sobmedida = 0;

            foreach($result as $item)
            {
                $acabamento = $item->acabamento;
                $tamanho = $item->tamanho;
                $quantidade = $item->quantidade;

                if($acabamento == 'IMA' && $tamanho == '15 X 15 CM')
                {
                    $quantidade_vendida_ima_15x15 += $quantidade;
                }

                if($acabamento == 'PAPEL MATTE' && $tamanho == '20 X 20 CM')
                {
                    $quantidade_vendida_poster_20x20 += $quantidade;
                }

                if($acabamento == 'PAPEL MATTE' && $tamanho == '30 X 30 CM')
                {
                    $quantidade_vendida_poster_30x30 += $quantidade;
                }

                if($acabamento == 'PAPEL MATTE' && $tamanho == '36 X 47,5 CM' || $acabamento == 'PAPEL MATTE' && $tamanho == '47,5 X 36 CM')
                {
                    $quantidade_vendida_poster_p += $quantidade;
                }

                if($acabamento == 'PAPEL MATTE' && $tamanho == '47,5 X 62,5 CM' || $acabamento == 'PAPEL MATTE' && $tamanho == '62,5 X 47,5 CM')
                {
                    $quantidade_vendida_poster_g += $quantidade;
                }

                if($acabamento == 'POSTER FILETE' && $tamanho == '20 X 20 CM')
                {
                    $quantidade_vendida_filete_20x20 += $quantidade;
                }

                if($acabamento == 'POSTER FILETE' && $tamanho == '30 X 30 CM')
                {
                    $quantidade_vendida_filete_30x30 += $quantidade;
                }

                if($acabamento == 'POSTER FILETE' && $tamanho == '36 X 47,5 CM' || $acabamento == 'POSTER FILETE' && $tamanho == '47,5 X 36 CM')
                {
                    $quantidade_vendida_filete_p += $quantidade;
                }

                if($acabamento == 'POSTER FILETE' && $tamanho == '47,5 X 62,5 CM' || $acabamento == 'POSTER FILETE' && $tamanho == '62,5 X 47,5 CM')
                {
                    $quantidade_vendida_filete_g += $quantidade;
                }
            }

            $data_acabamento = array('PAPEL MATTE', 'CANVAS');
            $data_tamanho = array('15 X 15 CM',
                '20 X 20 CM',
                '30 X 30 CM',
                '36 X 47,5 CM',
                '47,5 X 36 CM',
                '47,5 X 62,5 CM',
                '62,5 X 47,5 CM');

            $result_sobmedida = $this->relatorios_model->m_005_ranking_produtos_sobmedida($param, $data_acabamento, $data_tamanho);

            foreach($result_sobmedida as $item)
            {
                $acabamento = $item->acabamento;
                $tamanho = $item->tamanho;
                $quantidade = $item->quantidade;

                $quantidade_vendida_sobmedida += $quantidade;

            }

            $reposicao_ima_15x15 = '- ' . $quantidade_vendida_ima_15x15 . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_ima_15x15) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_ima_15x15) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_ima_15x15);

            $reposicao_poster_20x20 = '- ' . $quantidade_vendida_poster_20x20 . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_poster_20x20) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_poster_20x20) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_poster_20x20);
            $reposicao_poster_30x30 = '- ' . $quantidade_vendida_poster_30x30 . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_poster_30x30) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_poster_30x30) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_poster_30x30);
            $reposicao_poster_p = '- ' . $quantidade_vendida_poster_p . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_poster_p) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_poster_p) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_poster_p);
            $reposicao_poster_g = '- ' . $quantidade_vendida_poster_g . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_poster_g) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_poster_g) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_poster_g);

            $reposicao_filete_20x20 = '- ' . $quantidade_vendida_filete_20x20 . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_filete_20x20) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_filete_20x20) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_filete_20x20);
            $reposicao_filete_30x30 = '- ' . $quantidade_vendida_filete_30x30 . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_filete_30x30) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_filete_30x30) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_filete_30x30);
            $reposicao_filete_p = '- ' . $quantidade_vendida_filete_p . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_filete_p) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_filete_p) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_filete_p);
            $reposicao_filete_g = '- ' . $quantidade_vendida_filete_g . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_filete_g) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_poster_g) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_filete_g);

            $reposicao_sobmedida = '- ' . $quantidade_vendida_sobmedida . ' Vendidos - Curadoria (25%): ' . round(25 / 100 * $quantidade_vendida_sobmedida) . ' - Mais Vendidos (25%): ' . round(25 / 100 * $quantidade_vendida_sobmedida) . ' - Pedido automático (50%): ' . round(50 / 100 * $quantidade_vendida_sobmedida);

            echo $reposicao_ima_15x15.'|'.$reposicao_poster_20x20.'|'.$reposicao_poster_30x30.'|'.$reposicao_poster_p.'|'.$reposicao_poster_g.'|'.$reposicao_filete_20x20.'|'.$reposicao_filete_30x30.'|'.$reposicao_filete_p.'|'.$reposicao_filete_g.'|'.$reposicao_sobmedida;
        }
    }

    public function gerar_excel_reposicao()
    {
        // devolve o relatório filtrado
        $tipo_pedido = isset($_POST['tipo_pedido']) ? $_POST['tipo_pedido'] : 'PRONTA-ENTREGA';
        $ima_15x15 = isset($_POST['ima_15x15']) ? true : false;
        $poster_20x20 = isset($_POST['poster_20x20']) ? true : false;
        $poster_30x30 = isset($_POST['poster_30x30']) ? true : false;
        $poster_p = isset($_POST['poster_p']) ? true : false;
        $poster_g = isset($_POST['poster_g']) ? true : false;
        $filete_20x20 = isset($_POST['filete_20x20']) ? true : false;
        $filete_30x30 = isset($_POST['filete_30x30']) ? true : false;
        $filete_p = isset($_POST['filete_p']) ? true : false;
        $filete_g = isset($_POST['filete_g']) ? true : false;
        $sobmedida = isset($_POST['sobmedida']) ? true : false;

        $param = array('tipo_pedido' => $tipo_pedido, 'estoque' => 0);
        $data_acabamento = array();
        $data_tamanho = array();

        if($ima_15x15)
        {
            array_push($data_acabamento, 'IMA');
            array_push($data_tamanho, '15 X 15 CM');
        }

        if($poster_20x20)
        {
            array_push($data_acabamento, 'PAPEL MATTE');
            array_push($data_tamanho, '20 X 20 CM');
        }

        if($poster_30x30)
        {
            array_push($data_acabamento, 'PAPEL MATTE');
            array_push($data_tamanho, '30 X 30 CM');
        }

        if($poster_p)
        {
            array_push($data_acabamento, 'PAPEL MATTE');
            array_push($data_tamanho, '36 X 47,5 CM');
            array_push($data_tamanho, '47,5 X 36 CM');
        }

        if($poster_g)
        {
            array_push($data_acabamento, 'PAPEL MATTE');
            array_push($data_tamanho, '47,5 X 62,5 CM');
            array_push($data_tamanho, '62,5 X 47,5 CM');
        }

        if($filete_20x20)
        {
            array_push($data_acabamento, 'POSTER FILETE');
            array_push($data_tamanho, '20 X 20 CM');
        }

        if($filete_30x30)
        {
            array_push($data_acabamento, 'POSTER FILETE');
            array_push($data_tamanho, '30 X 30 CM');
        }

        if($filete_p)
        {
            array_push($data_acabamento, 'POSTER FILETE');
            array_push($data_tamanho, '36 X 47,5 CM');
            array_push($data_tamanho, '47,5 X 36 CM');
        }

        if($filete_g)
        {
            array_push($data_acabamento, 'POSTER FILETE');
            array_push($data_tamanho, '47,5 X 62,5 CM');
            array_push($data_tamanho, '62,5 X 47,5 CM');
        }

        /*
        if($sobmedida)
        {
            array_push($data_acabamento, 'CANVAS');
        }
        */

        array_unique($data_acabamento);
        array_unique($data_tamanho);

        $result = $this->relatorios_model->m_005_ranking_produtos($param, $data_acabamento, $data_tamanho);

        $sheet = new PHPExcel();
        $sheet->getProperties()->setTitle('Relatório de Reposição')->setDescription('Relatório de Reposição');

        $sheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Código')
            ->setCellValue('B1', 'Produto')
            ->setCellValue('C1', 'Tamanho')
            ->setCellValue('D1', 'Acabamento')
            ->setCellValue('E1', 'Data da Venda')
            ->setCellValue('F1', 'Quantidade')
            ->setCellValue('G1', 'Dias em Estoque')
            ->setCellValue('H1', 'Apelido');

        $i = 2;

        foreach($result as $item)
        {
            $codigo = $item->codigo;
            $produto = $item->produto;
            $tamanho = $item->tamanho;
            $acabamento = $item->acabamento;
            $data_venda = $item->data_venda;
            $quantidade = $item->quantidade;
            $dias_estoque = $item->dias_estoque;
            $estoque_atual = $item->estoque;

            //$apelido = 'SOBMEDIDA';

            if($acabamento == 'PAPEL MATTE' && $tamanho == '20 X 20 CM')
            {
                $apelido = 'POSTER 20X20';
            }

            if($acabamento == 'PAPEL MATTE' && $tamanho == '30 X 30 CM')
            {
                $apelido = 'POSTER 30X30';
            }

            if($acabamento == 'PAPEL MATTE' && $tamanho == '36 X 47,5 CM' || $acabamento == 'PAPEL MATTE' && $tamanho == '47,5 X 36 CM')
            {
                $apelido = 'POSTER P';
            }

            if($acabamento == 'PAPEL MATTE' && $tamanho == '47,5 X 62,5 CM' || $acabamento == 'PAPEL MATTE' && $tamanho == '62,5 X 47,5 CM')
            {
                $apelido = 'POSTER G';
            }

            if($acabamento == 'POSTER FILETE' && $tamanho == '20 X 20 CM')
            {
                $apelido = 'FILETE 20X20';
            }

            if($acabamento == 'POSTER FILETE' && $tamanho == '30 X 30 CM')
            {
                $apelido = 'FILETE 30X30';
            }

            if($acabamento == 'POSTER FILETE' && $tamanho == '36 X 47,5 CM' || $acabamento == 'POSTER FILETE' && $tamanho == '47,5 X 36 CM')
            {
                $apelido = 'FILETE P';
            }

            if($acabamento == 'POSTER FILETE' && $tamanho == '47,5 X 62,5 CM' || $acabamento == 'POSTER FILETE' && $tamanho == '62,5 X 47,5 CM')
            {
                $apelido = 'FILETE G';
            }

            $sheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $codigo)
                ->setCellValue('B'.$i, $produto)
                ->setCellValue('C'.$i, $tamanho)
                ->setCellValue('D'.$i, $acabamento)
                ->setCellValue('E'.$i, $data_venda)
                ->setCellValue('F'.$i, $quantidade)
                ->setCellValue('G'.$i, $dias_estoque)
                ->setCellValue('H'.$i, $apelido);
            $i++;
        }

        if($sobmedida)
        {
            $data_acabamento = array('PAPEL MATTE', 'CANVAS');
            $data_tamanho = array('15 X 15 CM',
                '20 X 20 CM',
                '30 X 30 CM',
                '36 X 47,5 CM',
                '47,5 X 36 CM',
                '47,5 X 62,5 CM',
                '62,5 X 47,5 CM');

            $result_sobmedida = $this->relatorios_model->m_005_ranking_produtos_sobmedida($param, $data_acabamento, $data_tamanho);

            foreach($result_sobmedida as $item)
            {
                $codigo = $item->codigo;
                $produto = $item->produto;
                $tamanho = $item->tamanho;
                $acabamento = $item->acabamento;
                $data_venda = $item->data_venda;
                $quantidade = $item->quantidade;
                $dias_estoque = $item->dias_estoque;
                $estoque_atual = $item->estoque;

                $apelido = 'SOBMEDIDA';

                $sheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $codigo)
                    ->setCellValue('B'.$i, $produto)
                    ->setCellValue('C'.$i, $tamanho)
                    ->setCellValue('D'.$i, $acabamento)
                    ->setCellValue('E'.$i, $data_venda)
                    ->setCellValue('F'.$i, $quantidade)
                    ->setCellValue('G'.$i, $dias_estoque)
                    ->setCellValue('H'.$i, $apelido);
                $i++;
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="relatorio_reposicao.xls"');
        header('Cache-Control: max-age=0');

        header('Cache-Control: max-age=1');

        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
        $objWriter->save('php://output');
        exit;

    }

    public function gerar_excel_estoque()
    {
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite'] = TRUE;
        $config['remove_spaces'] = FALSE;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('planilha_excel'))
        {
            $this->session->set_flashdata('error_upload', strip_tags($this->upload->display_errors()));
            redirect('/relatorios/estoque');
        }
        else
        {
            // limpa a tabela antes de importar
            $this->relatorios_model->truncate('millennium_012_estoque_disponivel');

            // importa a planilha
            $inputFileName = $_FILES['planilha_excel']['name'];
            $inputFileType = 'Excel2007';
            $upload_path = './upload/';

            $this->load->library('PHPExcel');
            $inputFileType = PHPExcel_IOFactory::identify($upload_path . $inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);

            $arr_data = array();

            $objPHPExcel        = $objReader->load($upload_path . $inputFileName);
            $total_sheets       = $objPHPExcel->getSheetCount(); // here 4
            $allSheetName       = $objPHPExcel->getSheetNames(); // array ([0]=>'student',[1]=>'teacher',[2]=>'school',[3]=>'college')
            $objWorksheet       = $objPHPExcel->setActiveSheetIndex(0);
            $highestRow         = $objWorksheet->getHighestRow(); // here 5
            $highestColumn      = $objWorksheet->getHighestColumn(); // here 'E'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  // here 5

            for ($row = 1; $row <= $highestRow; ++$row)
            {
                for ($col = 0; $col <= $highestColumnIndex; ++$col)
                {
                    $value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();

                    if(is_array($arr_data))
                    {
                        $arr_data[$row-1][$col]=$value;
                    }
                }
            }

            $linha = 1;

            foreach($arr_data as $data)
            {
                if($linha>1)
                {
                    $codigo = $data[0];
                    $produto = $data[1];
                    $tamanho = $data[2];
                    $acabamento = $data[3];
                    $fantasia = $data[4];
                    $estoque = $data[5];

                    $data = array('codigo' => $codigo,
                        'produto' => $produto,
                        'tamanho' => $tamanho,
                        'acabamento' => $acabamento,
                        'loja' => $fantasia,
                        'estoque' => $estoque);

                    $this->relatorios_model->set($data, 'millennium_012_estoque_disponivel');
                }

                $linha++;
            }

            // devolve o relatório filtrado
            $tipo_pedido = isset($_POST['tipo_pedido']) ? $_POST['tipo_pedido'] : 'PRONTA-ENTREGA';
            $ima_15x15 = isset($_POST['ima_15x15']) ? true : false;
            $poster_20x20 = isset($_POST['poster_20x20']) ? true : false;
            $poster_30x30 = isset($_POST['poster_30x30']) ? true : false;
            $poster_p = isset($_POST['poster_p']) ? true : false;
            $poster_g = isset($_POST['poster_g']) ? true : false;
            $filete_20x20 = isset($_POST['filete_20x20']) ? true : false;
            $filete_30x30 = isset($_POST['filete_30x30']) ? true : false;
            $filete_p = isset($_POST['filete_p']) ? true : false;
            $filete_g = isset($_POST['filete_g']) ? true : false;
            $sobmedida = isset($_POST['sobmedida']) ? true : false;

            $param = array();
            $data_acabamento = array();
            $data_tamanho = array();

            if($ima_15x15)
            {
                array_push($data_acabamento, 'IMA');
                array_push($data_tamanho, '15 X 15 CM');
            }

            if($poster_20x20)
            {
                array_push($data_acabamento, 'PAPEL MATTE');
                array_push($data_tamanho, '20 X 20 CM');
            }

            if($poster_30x30)
            {
                array_push($data_acabamento, 'PAPEL MATTE');
                array_push($data_tamanho, '30 X 30 CM');
            }

            if($poster_p)
            {
                array_push($data_acabamento, 'PAPEL MATTE');
                array_push($data_tamanho, '36 X 47,5 CM');
                array_push($data_tamanho, '47,5 X 36 CM');
            }

            if($poster_g)
            {
                array_push($data_acabamento, 'PAPEL MATTE');
                array_push($data_tamanho, '47,5 X 62,5 CM');
                array_push($data_tamanho, '62,5 X 47,5 CM');
            }

            if($filete_20x20)
            {
                array_push($data_acabamento, 'POSTER FILETE');
                array_push($data_tamanho, '20 X 20 CM');
            }

            if($filete_30x30)
            {
                array_push($data_acabamento, 'POSTER FILETE');
                array_push($data_tamanho, '30 X 30 CM');
            }

            if($filete_p)
            {
                array_push($data_acabamento, 'POSTER FILETE');
                array_push($data_tamanho, '36 X 47,5 CM');
                array_push($data_tamanho, '47,5 X 36 CM');
            }

            if($filete_g)
            {
                array_push($data_acabamento, 'POSTER FILETE');
                array_push($data_tamanho, '47,5 X 62,5 CM');
                array_push($data_tamanho, '62,5 X 47,5 CM');
            }

            if($sobmedida)
            {
                array_push($data_acabamento, 'CANVAS');
            }

            array_unique($data_acabamento);
            array_unique($data_tamanho);

            $result = $this->relatorios_model->m_012_estoque_disponivel(false, $data_acabamento, $data_tamanho);

            $sheet = new PHPExcel();
            $sheet->getProperties()->setTitle('Relatório de Estoque')->setDescription('Relatório de Estoque');

            $sheet->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Código')
                ->setCellValue('B1', 'Produto')
                ->setCellValue('C1', 'Tamanho')
                ->setCellValue('D1', 'Acabamento')
                ->setCellValue('E1', 'Loja')
                ->setCellValue('F1', 'Estoque')
                ->setCellValue('G1', 'Apelido');

            $i = 2;

            foreach($result as $item)
            {
                $codigo = $item->codigo;
                $produto = $item->produto;
                $tamanho = $item->tamanho;
                $acabamento = $item->acabamento;
                $loja = $item->loja;
                $estoque = $item->estoque;

                $apelido = 'SOBMEDIDA';

                if($acabamento == 'PAPEL MATTE' && $tamanho == '20 X 20 CM')
                {
                    $apelido = 'POSTER 20X20';
                }

                if($acabamento == 'PAPEL MATTE' && $tamanho == '30 X 30 CM')
                {
                    $apelido = 'POSTER 30X30';
                }

                if($acabamento == 'PAPEL MATTE' && $tamanho == '36 X 47,5 CM' || $acabamento == 'PAPEL MATTE' && $tamanho == '47,5 X 36 CM')
                {
                    $apelido = 'POSTER P';
                }

                if($acabamento == 'PAPEL MATTE' && $tamanho == '47,5 X 62,5 CM' || $acabamento == 'PAPEL MATTE' && $tamanho == '62,5 X 47,5 CM')
                {
                    $apelido = 'POSTER G';
                }

                if($acabamento == 'POSTER FILETE' && $tamanho == '20 X 20 CM')
                {
                    $apelido = 'FILETE 20X20';
                }

                if($acabamento == ' POSTER FILETE' && $tamanho == '30 X 30 CM')
                {
                    $apelido = 'FILETE 30X30';
                }

                if($acabamento == 'POSTER FILETE' && $tamanho == '36 X 47,5 CM' || $acabamento == 'POSTER FILETE' && $tamanho == '47,5 X 36 CM')
                {
                    $apelido = 'FILETE P';
                }

                if($acabamento == 'POSTER FILETE' && $tamanho == '47,5 X 62,5 CM' || $acabamento == 'POSTER FILETE' && $tamanho == '62,5 X 47,5 CM')
                {
                    $apelido = 'FILETE G';
                }

                $sheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $codigo)
                    ->setCellValue('B'.$i, $produto)
                    ->setCellValue('C'.$i, $tamanho)
                    ->setCellValue('D'.$i, $acabamento)
                    ->setCellValue('E'.$i, $loja)
                    ->setCellValue('F'.$i, $estoque)
                    ->setCellValue('G'.$i, $apelido);

                $i++;
            }

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="relatorio_estoque.xls"');
            header('Cache-Control: max-age=0');

            header('Cache-Control: max-age=1');

            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }
}