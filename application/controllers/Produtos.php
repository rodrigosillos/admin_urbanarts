<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('produtos_model', 'pedidos_model'));
	}
	
	public function index()
	{
		//code
	}
	
	public function mais_vendidos()
	{
		$data = array('usuario_sessao' => $this->usuario_sessao);
		$this->load->view('produtos_mais_vendidos', $data);
	}
	
	public function exporta_mais_vendidos()
	{
		$this->output->enable_profiler(FALSE);
		
		if($_POST)
		{
			$sheet = new PHPExcel();
			$sheet->getProperties()->setTitle('Mais Vendidos')->setDescription('Mais Vendidos');
			
			$sheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'Produto')
			->setCellValue('B1', 'Acabamento')
			->setCellValue('C1', 'Tamanho')
			->setCellValue('D1', 'Quantidade Vendida')
			->setCellValue('E1', 'Média Venda x Mês')
			->setCellValue('F1', 'Lojas');
			
			$arr_data_inicio = explode('/', $_POST['data_inicio']);
			$data_inicio = $arr_data_inicio[2] .'-'. $arr_data_inicio[1] .'-'. $arr_data_inicio[0] . ' 00:00:00';
			
			$arr_data_final = explode('/', $_POST['data_final']);
			$data_final = $arr_data_final[2] .'-'. $arr_data_final[1] .'-'. $arr_data_final[0] . ' 23:59:59';
			
			$limite = $_POST['limite'];
			
			//$data_inicio = '2016-10-01 00:00:00';
			//$data_final = '2016-10-31 23:59:59';
			
			$produtos = $this->pedidos_model->produtos_mais_vendidos($data_inicio, $data_final, $limite);
			
			$i = 2;
			
			foreach($produtos as $produto)
			{
				$produto_id = $produto->id;
				$produto_nome = $produto->nome;
				$produto_data = $produto->data_cadastro;
				$quantidade = $produto->quantidade;
				//$quantidade_lojas = $produto->quantidade_lojas;
				
				$contagem_result = $this->pedidos_model->contagem_lojas($data_inicio, $data_final, $produto_id);
				
				if($contagem_result)
				{
					$quantidade_lojas = $contagem_result[0]->quantidade_lojas;
				}
				
				$d1 = $produto_data;
				$d2 = date('Y-m-d H:i:s');
				
				$diferenca_mes = (int)abs((strtotime($d1) - strtotime($d2))/(60*60*24*30));
				
				if($diferenca_mes > 12)
				{
					$diferenca_mes = 12;
				}
				
				if($diferenca_mes<>0)
				{
					$media_mes = round($quantidade / $diferenca_mes);
				}			
				
				$sheet->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $produto_nome)
				->setCellValue('B'.$i, '')
				->setCellValue('C'.$i, '')
				->setCellValue('D'.$i, $quantidade)
				->setCellValue('E'.$i, $media_mes)
				->setCellValue('F'.$i, $quantidade_lojas);
				
				$sheet->getActiveSheet(0)->getStyle('A'.$i)->applyFromArray(
					array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
										  'color' => array('rgb' => 'ec008c'))));
				
				$sheet->getActiveSheet(0)->getStyle('B'.$i)->applyFromArray(
					array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
										  'color' => array('rgb' => 'ec008c'))));
				
				$sheet->getActiveSheet(0)->getStyle('C'.$i)->applyFromArray(
					array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
										  'color' => array('rgb' => 'ec008c'))));			
				
				$sheet->getActiveSheet(0)->getStyle('D'.$i)->applyFromArray(
					array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
										  'color' => array('rgb' => 'ec008c'))));
				
				$sheet->getActiveSheet(0)->getStyle('E'.$i)->applyFromArray(
					array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
										  'color' => array('rgb' => 'ec008c'))));
				
				$sheet->getActiveSheet(0)->getStyle('F'.$i)->applyFromArray(
					array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
										  'color' => array('rgb' => 'ec008c'))));
				
				$i++;
				
				$vendas = $this->pedidos_model->desmembra_venda($data_inicio, $data_final, $produto_id);
				
				foreach($vendas as $venda)
				{
					$acabamento = $venda->acabamento;
					$tamanho = $venda->tamanho;
					$quantidade_desmembrada = $venda->quantidade;
					
					$sheet->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $produto_nome)
					->setCellValue('B'.$i, $acabamento)
					->setCellValue('C'.$i, $tamanho)
					->setCellValue('D'.$i, $quantidade_desmembrada)
					->setCellValue('E'.$i, '')
					->setCellValue('F'.$i, '');
					
					$i++;
				}
				
			}
			
			$data_periodo = $data_inicio .'_'. $data_final;
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="relatorio_mais_vendidos_'.$data_periodo.'.xls"');
			header('Cache-Control: max-age=0');
			
			header('Cache-Control: max-age=1');
			
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			
			$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}
	}
	
	public function carga_estoque()
	{
		$inputFileName = 'Reserva Mix - Ponteio - 2016.xlsm';
		$inputFileType = 'Excel2007';
		$upload_path = './upload/';
		
		$this->load->library('PHPExcel');
		$inputFileType = PHPExcel_IOFactory::identify($upload_path . $inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		
		$arr_data = array();
		
		$objPHPExcel        = $objReader->load($upload_path . $inputFileName);
		$total_sheets       = $objPHPExcel->getSheetCount(); // here 4
		$allSheetName       = $objPHPExcel->getSheetNames(); // array ([0]=>'student',[1]=>'teacher',[2]=>'school',[3]=>'college')
		$objWorksheet       = $objPHPExcel->setActiveSheetIndex(0);
		$highestRow         = $objWorksheet->getHighestRow(); // here 5
		$highestColumn      = $objWorksheet->getHighestColumn(); // here 'E'
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  // here 5
		
		for ($row = 1; $row <= $highestRow; ++$row)
		{
			for ($col = 0; $col <= $highestColumnIndex; ++$col)
			{
				$value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
				
				if(is_array($arr_data))
				{
					$arr_data[$row-1][$col]=$value;
				}
			}
		}
		
		$linha = 1;
		
		foreach($arr_data as $data)
		{
			if($linha>3)
			{
				$produto_codigo = $data[0];
				$carga_estoque = $data[6];
				
				if($produto_codigo)
				{
					$data_select = array('id', 'estoque');
					$data_where = array('cod_produto_millennium' => $produto_codigo);
					
					$produto_result = $this->produtos_model->get($data_select, 'produtos_produto', $data_where);
					
					if($produto_result)
					{
						foreach($produto_result as $produto)
						{
							$produto_id = $produto->id;
							$produto_estoque_atual = $produto->estoque;
							
							$nova_carga_estoque = ($produto_estoque_atual+$carga_estoque);
							
							//echo $produto_codigo . ' - atual '. $produto_estoque_atual . ' - quantidade enxoval ' . $carga_estoque . ' - nova carga ' .$nova_carga_estoque. '<br/>';
							$this->produtos_model->update($produto_id, array('estoque' => $nova_carga_estoque, 'ativo' => 1));
							//$this->produtos_model->update($produto_id, array('ativo' => 1));
						}
					}
					else
					{
						echo 'produto '.$produto_codigo.' não encontrado!<br/>';
					}
				}		
			}			
			$linha++;
		}
	}
}