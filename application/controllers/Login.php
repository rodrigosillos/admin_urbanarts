<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('usuarios_model'));
	}
	
	public function index()
	{
		$this->load->view('login');
	}
	
	public function auth()
	{
		$email = !empty($_POST['email']) ? $_POST['email'] : false;
		$senha = !empty($_POST['senha']) ? $_POST['senha'] : false;
		
		$retorno = 'nao_logado';
		
		if($email && $senha)
		{
			$data_auth = array('email' => $email,
							   'senha' => md5($senha),
							   'ativo' => 1);
			
			$data_usuario = $this->usuarios_model->auth($data_auth);
			
			if($data_usuario)
			{
				$retorno = 'logado';		
				$this->session->set_userdata('usuario_sessao', $data_usuario);
			}
		}
		
		echo $retorno;
	}
}
