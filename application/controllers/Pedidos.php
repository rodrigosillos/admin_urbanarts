<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('lojas_model', 'carrinho_model', 'produtos_model', 'pedidos_model'));
	}
	
	public function carrinho()
	{
		$lojas = $this->lojas_model->get();
		
		$data = array('lojas' => $lojas,
					  'usuario_sessao' => $this->usuario_sessao);
		
		$this->load->view('pedidos_carrinho', $data);
	}
	
	public function carrega_carrinho()
	{
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'xls|xlsx';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = FALSE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('planilha_excel'))
		{
			$this->session->set_flashdata('error_upload', strip_tags($this->upload->display_errors()));
			redirect('/pedidos/carrinho');
		}
		else
		{			
			// importa a planilha
			$inputFileName = $_FILES['planilha_excel']['name'];
			$inputFileType = 'Excel2007';
			$upload_path = './upload/';
			
			$this->load->library('PHPExcel');
			$inputFileType = PHPExcel_IOFactory::identify($upload_path . $inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			
			$arr_data = array();
			
			$objPHPExcel        = $objReader->load($upload_path . $inputFileName);
			$total_sheets       = $objPHPExcel->getSheetCount(); // here 4
			$allSheetName       = $objPHPExcel->getSheetNames(); // array ([0]=>'student',[1]=>'teacher',[2]=>'school',[3]=>'college')
			$objWorksheet       = $objPHPExcel->setActiveSheetIndex(0);
			$highestRow         = $objWorksheet->getHighestRow(); // here 5
			$highestColumn      = $objWorksheet->getHighestColumn(); // here 'E'
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  // here 5
			
			for ($row = 1; $row <= $highestRow; ++$row)
			{
				for ($col = 0; $col <= $highestColumnIndex; ++$col)
				{
					$value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
					
					if(is_array($arr_data))
					{
						$arr_data[$row-1][$col]=$value;
					}
				}
			}
			
			$loja = isset($_POST['loja']) ? $_POST['loja'] : false;
			$carrinho = false;
			$error_carrinho = array();
			
			if($loja)
			{
				$carrinho = $this->carrinho_model->get(array('cliente_id' => $loja));
			}
			else
			{
				$erro = 'Loja não encontrada.';
				array_push($error_carrinho, $erro);
			}
			
			if($carrinho)
			{
				$carrinho_id = $carrinho[0]->id;
				
				$linha = 1;
				
				foreach($arr_data as $data)
				{
					if($linha>1)
					{
						$produto_codigo = $data[0];
						$tamanho = $data[2];
						$acabamento = $data[3];
						$quantidade = $data[5];
						
						$produto_codigo = str_pad($produto_codigo, 6, '0', STR_PAD_LEFT);
						
						$produto = $this->produtos_model->get(array('id'), false, array('cod_produto_millennium' => $produto_codigo));
						
						if($produto)
						{
							$produto_id = $produto[0]->id;
							
							$tipo_pedido = 'reposicao_sobmedida';
							
							if($acabamento == 'IMA')
							{
								$tipo_pedido = 'reposicao_ima';
								$acabamento_id = 7;
							}
							
							if($acabamento == 'POSTER FILETE')
							{
								$tipo_pedido = 'reposicao_filete';
								$acabamento_id = 4;
							}
							
							if($acabamento == 'PAPEL MATTE' && $tamanho == '20 X 20 CM'
							|| $acabamento == 'PAPEL MATTE' && $tamanho == '30 X 30 CM'
							|| $acabamento == 'PAPEL MATTE' && $tamanho == '36 X 47,5 CM'
							|| $acabamento == 'PAPEL MATTE' && $tamanho == '47,5 X 36 CM'
							|| $acabamento == 'PAPEL MATTE' && $tamanho == '47,5 X 62,5 CM'
							|| $acabamento == 'PAPEL MATTE' && $tamanho == '62,5 X 47,5 CM')
							{
								$tipo_pedido = 'reposicao_poster';
								$acabamento_id = 3;
							}
							
							if($acabamento == 'CANVAS')
							{
								$tipo_pedido = 'reposicao_sobmedida';
								$acabamento_id = 1;
							}
							
							if($acabamento == 'PAPEL MATTE' && $tamanho != '20 X 20 CM'
							&& $acabamento == 'PAPEL MATTE' && $tamanho != '30 X 30 CM'
							&& $acabamento == 'PAPEL MATTE' && $tamanho != '36 X 47,5 CM'
							&& $acabamento == 'PAPEL MATTE' && $tamanho != '47,5 X 36 CM'
							&& $acabamento == 'PAPEL MATTE' && $tamanho != '47,5 X 62,5 CM'
							&& $acabamento == 'PAPEL MATTE' && $tamanho != '62,5 X 47,5 CM')
							{
								$tipo_pedido = 'reposicao_sobmedida';
								$acabamento_id = 3;
							}							
							
							$arr_tamanho = explode('X', $tamanho);
							$tam_largura = $arr_tamanho[0];
							$tam_altura = $arr_tamanho[1];
							
							$tam_largura = preg_replace('/\s+/', '', $tam_largura);
							$tam_altura = preg_replace('/\s+/', '', $tam_altura);
							
							$tam_largura = str_replace('CM', '', $tam_largura);
							$tam_altura = str_replace('CM', '', $tam_altura);
							
							$tamanho_descricao = $tam_largura.'cm x '.$tam_altura.'cm';
							
							$tamanho = $this->produtos_model->get(array('id', 'id_velho'), 'produtos_tamanho_novo', array('titulo_ptbr' => $tamanho_descricao));
							
							if($tamanho)
							{
								$tamanho_id = $tamanho[0]->id;
								$tamanho_id_velho = $tamanho[0]->id_velho;
							}
							else
							{
								$erro = 'Tamanho: ' . $tamanho_id_velho . ' não encontrado.';
								array_push($error_carrinho, $erro);
							}
							
							$data_preco = array('produto_id' => $produto_id,
												'acabamento_id' => $acabamento_id,
												'tamanho_id' => $tamanho_id_velho);
							
							$preco = $this->produtos_model->get(array('id', 'valor', 'custo'), 'produtos_preco', $data_preco);
							
							if($preco)
							{
								$preco_id = $preco[0]->id;
								$valor_venda = $preco[0]->valor;
								$valor_custo = $preco[0]->custo;
								
								$valor_artista = $valor_venda * 0.10 * $quantidade;
								$valor_marketing = $valor_venda * 0.02 * $quantidade;
								$valor_royalties = $valor_venda * 0.18 * $quantidade;
								
								$total_custo = $valor_custo * $quantidade;
								$total_custo_producao = $valor_custo * $quantidade + $valor_artista;
							}
							else
							{
								$erro = 'Produto: ' . $produto_id . ' preço não encontrado.';
								array_push($error_carrinho, $erro);
							}
							
							$data_item = array('tipo_pedido' => $tipo_pedido,
											   'carrinho_id' => $carrinho_id,
											   'produto_id' => $produto_id,
											   'acabamento_id' => $acabamento_id,
											   'tamanho_id' => $tamanho_id_velho,
											   'preco_id' => $preco_id,
											   'quantidade' => $quantidade,
											   'valor_artista' => $valor_artista,
											   'valor_marketing' => $valor_marketing,
											   'valor_royalties' => $valor_royalties,
											   'total_custo' => $total_custo,
											   'total_custo_producao' => $total_custo_producao);
							
							//print_r($data_item);
							$this->carrinho_model->set($data_item, 'carrinho_item_novo');
						}
						else
						{
							$erro = 'Produto: ' . $produto_id . ' não encontrado.';
							array_push($error_carrinho, $erro);
						}
					}
					$linha++;
				}
			}
			else
			{
				$erro = 'Loja não tem carrinho';
				array_push($error_carrinho, $erro);
			}
			
			$data_carrinho = array('error_carrinho' => $error_carrinho,
								   'usuario_sessao' => $this->usuario_sessao);
			
			$this->load->view('carrega_carrinho_result', $data_carrinho);
		}
	}
	
	public function novo()
	{
		$lojas = $this->lojas_model->get();
		$transportadoras = $this->pedidos_model->get(array('id', 'razao_social'), 'fretes_transportadora');
		
		$data = array('lojas' => $lojas,
					  'transportadoras' => $transportadoras,
					  'usuario_sessao' => $this->usuario_sessao);
		
		$this->load->view('pedidos_novo', $data);
	}
	
	public function novo_pedido()
	{
		// importa a planilha
		//$inputFileName = $_FILES['planilha_excel']['name'];
		$inputFileName = 'Robô - Enxoval Piracicaba - 29 09 16 - Faltantes.xlsx';
		$inputFileType = 'Excel2007';
		$upload_path = './upload/';
		
		$this->load->library('PHPExcel');
		$inputFileType = PHPExcel_IOFactory::identify($upload_path . $inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		
		$arr_data = array();
		
		$objPHPExcel        = $objReader->load($upload_path . $inputFileName);
		$total_sheets       = $objPHPExcel->getSheetCount(); // here 4
		$allSheetName       = $objPHPExcel->getSheetNames(); // array ([0]=>'student',[1]=>'teacher',[2]=>'school',[3]=>'college')
		
		for ($sheet = 0; $sheet <= ($total_sheets-1); $sheet++)
		{
			if($sheet >= 1)
			{
				$objWorksheet       = $objPHPExcel->setActiveSheetIndex($sheet);
				$highestRow         = $objWorksheet->getHighestRow(); // here 5
				$highestColumn      = $objWorksheet->getHighestColumn(); // here 'E'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  // here 5
				
				for ($row = 1; $row <= $highestRow; ++$row)
				{
					for ($col = 0; $col <= $highestColumnIndex; ++$col)
					{
						$value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
						
						if(is_array($arr_data))
						{
							$arr_data[$row-1][$col]=$value;
						}
					}
				}
				
				//$loja_id = isset($_POST['loja']) ? $_POST['loja'] : false;
				$loja_id = 21604;
				$error_pedido = array();
				
				if($loja_id)
				{
					$loja_result = $this->lojas_model->get_by(array('id' => $loja_id));
					
					if($loja_result)
					{
						$now = date("Y-m-d H:i:s");
						
						$cliente_id = $loja_result[0]->id;
						$cliente = $loja_result[0]->nome;
						$cpf = $loja_result[0]->cpf;
						$email = $loja_result[0]->email;
						$cep = $loja_result[0]->cep;
						$endereco = $loja_result[0]->endereco;
						$numero = $loja_result[0]->numero;
						$complemento = $loja_result[0]->complemento;
						$referencia = $loja_result[0]->referencia;
						$bairro = $loja_result[0]->bairro;
						$cidade = $loja_result[0]->cidade;
						$telefone = $loja_result[0]->telefone;
						$estado = $loja_result[0]->estado;
						
						$data_pedido = array('data' => $now,
											 'fechamento' => $now,
											 'status' => 'AGUARDANDO PAGAMENTO',
											 'metodo_pagamento' => 'SHOPLINE',
											 'eh_poucanca' => 0,
											 'parcelas' => 1,
											 'tipo_servico' => '',
											 'peso_total' => 0.00,
											 'custo_envio' => 0.00,
											 'cliente_id' => $cliente_id,
											 'cliente' => $cliente,
											 'tipo_cliente' => 'PF',
											 'cpf' => $cpf,
											 'email' => $email,
											 'telefone' => $telefone,
											 'cobranca_cep' => $cep,
											 'cobranca_tipo_logradouro' => 'Residencial',
											 'cobranca_logradouro' => $endereco,
											 'cobranca_numero' => $numero,
											 'cobranca_complemento' => $complemento,
											 'cobranca_bairro' => $bairro,
											 'cobranca_cidade' => $cidade,
											 'cobranca_estado' => $estado,
											 'entrega_cep' => $cep,
											 'entrega_tipo_logradouro' => 'Residencial',
											 'entrega_logradouro' => $endereco,
											 'entrega_numero' => $numero,
											 'entrega_complemento' => $complemento,
											 'entrega_cidade' => $cidade,
											 'entrega_bairro' => $bairro,
											 'entrega_estado' => $estado,
											 'valor_cupom' => 0.00,
											 'total_itens' => 0.00,
											 'total' => 0.00,
											 'cupom_gerado' => 0,
											 'tipo_pedido' => '',
											 'tipo_unico' => 'loja',
											 'pedido_pai' => null,
											 'prazo_envio' => '2016-11-20 00:00:00',
											 'transportadora_id' => 6,
											 'transportadora_razao_social' => 'Retirada Central UA');
						
						$pedido_id = $this->pedidos_model->set($data_pedido, 'pedidos_pedido');
					}				
				}
				else
				{
					$erro = 'Loja não encontrada.';
					array_push($error_pedido, $erro);
				}
				
				$linha = 1;
				
				$arr_size = array('matte30x30',
								  'matte36x475',
								  'matte475x625',
								  'filete30x30',
								  'filete36x475',
								  'filete475x625',
								  'ima15x15',
								  'filete20x20',
								  'matte20x20');
				
				$tipo_pedido_preco = 'reposicao';
				$tipo_pedido = 'natal';
				
				$valor_total_pedido = 0.00;
				$valor_total_custo_pedido = 0.00;
				
				foreach($arr_data as $data)
				{
					if($linha>8)
					{
						$arte_xls = addslashes($data[3]);
						
						$quantidade_filete30x30 = $data[10]; // Coluna K
						$quantidade_filete36x475 = $data[11]; // Coluna L
						$quantidade_filete475x625 = $data[12]; // Coluna M
						
						$quantidade_matte30x30 = $data[14]; // Coluna O
						$quantidade_matte36x475 = $data[15]; // Coluna P
						$quantidade_matte475x625 = $data[16]; // Coluna Q
						
						$quantidade_ima15x15 = $data[18]; // Coluna S
						
						$quantidade_filete20x20 = $data[23]; // Coluna X
						$quantidade_matte20x20 = $data[24]; // Coluna Y
						
						if(!empty($arte_xls))
						{
							foreach($arr_size as $size)
							{
								$quantidade_xls = ${"quantidade_$size"};
								
								if($quantidade_xls!="0" and $quantidade_xls!="")
								{
									// produto
									$produto_result = $this->produtos_model->get_utf8($arte_xls);
									
									if($produto_result)
									{
										$produto_id = $produto_result[0]->id;
										$produto_nome = addslashes($produto_result[0]->titulo_ptbr);
										$artista_id = $produto_result[0]->artista_id;
										$formato_id = $produto_result[0]->formato_id;
										
										// artista
										$artista_result = $this->lojas_model->get_artista(array('id' => $artista_id));
										
										if($artista_result)
										{
											$artista_nome = $artista_result[0]->nome;
										}
										
										// imagem
										$trabalho_result = $this->produtos_model->get(array('imagem'), 'trabalhos_trabalho', array('produto_id' => $produto_id));
										
										if($trabalho_result)
										{
											$produto_imagem = $trabalho_result[0]->imagem;
										}
										
										$inserir_produto = false;
										$error_inserir_produto = array();
										
										if($size == "filete30x30")
										{
											$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(4), array(17));
											
											if($preco_result)
											{
												//$tipo_pedido = 'natal_filete';
												$tipo_pedido = 'reposicao_filete';
												$valor_unitario = $preco_result[0]->valor;
												$valor_custo = $preco_result[0]->custo;
												
												if($tipo_pedido_preco=='reposicao')
												{
													$valor_custo = $preco_result[0]->custo_reposicao;
												}
												
												$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Quadrado) 30cm x 30cm";
												
												$inserir_produto = true;
											}
											else
											{
												$data_error = array('produto_id' => $produto_id,
																	'produto_nome' => $produto_nome,
																	'acabamento_id' => 4,
																	'tamanho_id' => 17);
												
												array_push($error_inserir_produto, $data_error);
											}
											
										}
										
										if($size == "filete36x475")
										{
											$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(4), array(1, 18));
											
											if($preco_result)
											{
												//$tipo_pedido = 'natal_filete';
												$tipo_pedido = 'reposicao_filete';
												$valor_unitario = $preco_result[0]->valor;
												$valor_custo = $preco_result[0]->custo;
												
												if($tipo_pedido_preco=='reposicao')
												{
													$valor_custo = $preco_result[0]->custo_reposicao;
												}
												
												$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Retrato) 36cm x 47,5cm";
												
												if($formato_id == 2)
												{
												  $produto_descricao = "Acabamento: Poster Filete \nTamanho: (Paisagem) 47,5cm x 36cm";
												}
												
												$inserir_produto = true;
											}
											else
											{
												$data_error = array('produto_id' => $produto_id,
																	'produto_nome' => $produto_nome,
																	'acabamento_id' => 4,
																	'tamanho_id' => array(1, 18));
												
												array_push($error_inserir_produto, $data_error);
											}										
											
										}
										
										if($size == "filete475x625")
										{
											$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(4), array(2, 19));
											
											if($preco_result)
											{
												//$tipo_pedido = 'natal_filete';
												$tipo_pedido = 'reposicao_filete';
												$valor_unitario = $preco_result[0]->valor;
												$valor_custo = $preco_result[0]->custo;
												
												if($tipo_pedido_preco=='reposicao')
												{
													$valor_custo = $preco_result[0]->custo_reposicao;	
												}
												
												$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Retrato) 47,5cm x 62,5cm";
												
												if($formato_id == 2)
												{
													$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Paisagem) 62,5cm x 47,5cm";
												}
												
												$inserir_produto = true;
											}
											else
											{
												$data_error = array('produto_id' => $produto_id,
																	'produto_nome' => $produto_nome,
																	'acabamento_id' => 4,
																	'tamanho_id' => array(2, 19));
												
												array_push($error_inserir_produto, $data_error);
											}										
											
										}
										
										if($size == "matte30x30")
										{										
											$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(3), array(17));
											
											if($preco_result)
											{
												//$tipo_pedido = 'natal_poster';
												$tipo_pedido = 'reposicao_poster';
												$valor_unitario = $preco_result[0]->valor;
												$valor_custo = $preco_result[0]->custo;
												
												if($tipo_pedido_preco=='reposicao')
												{
													$valor_custo = $preco_result[0]->custo_reposicao;	
												}
												
												$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Quadrado) 30cm x 30cm";
												
												$inserir_produto = true;
											}
											else
											{
												$data_error = array('produto_id' => $produto_id,
																	'produto_nome' => $produto_nome,
																	'acabamento_id' => 3,
																	'tamanho_id' => 17);
												
												array_push($error_inserir_produto, $data_error);
											}										
											
										}
										
										if($size == "matte36x475")
										{
											$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(3), array(1, 18));
											
											if($preco_result)
											{
												//$tipo_pedido = 'natal_poster';
												$tipo_pedido = 'reposicao_poster';
												$valor_unitario = $preco_result[0]->valor;
												$valor_custo = $preco_result[0]->custo;
												
												if($tipo_pedido_preco=='reposicao')
												{
													$valor_custo = $preco_result[0]->custo_reposicao;	
												}
												
												$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Retrato) 36cm x 47,5cm";
												
												if($formato_id == 2)
												{
													$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Paisagem) 47,5cm x 36cm";
												}
												
												$inserir_produto = true;
											}
											else
											{
												$data_error = array('produto_id' => $produto_id,
																	'produto_nome' => $produto_nome,
																	'acabamento_id' => 3,
																	'tamanho_id' => array(1, 18));
												
												array_push($error_inserir_produto, $data_error);
											}										
											
										}
										
										if($size == "matte475x625")
										{
											$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(3), array(2, 19));
											
											if($preco_result)
											{
												//$tipo_pedido = 'natal_poster';
												$tipo_pedido = 'reposicao_poster';
												$valor_unitario = $preco_result[0]->valor;
												$valor_custo = $preco_result[0]->custo;
												
												if($tipo_pedido_preco=='reposicao')
												{
													$valor_custo = $preco_result[0]->custo_reposicao;	
												}
												
												$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Retrato) 47,5cm x 62,5cm";
												
												if($formato_id == 2)
												{
													$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Paisagem) 62,5cm x 47,5cm";
												}
												
												$inserir_produto = true;
											}
											else
											{
												$data_error = array('produto_id' => $produto_id,
																	'produto_nome' => $produto_nome,
																	'acabamento_id' => 3,
																	'tamanho_id' => array(2, 19));
												
												array_push($error_inserir_produto, $data_error);
											}										
											
										}
										
										if($size == "ima15x15")
										{										
											$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(7), array(43));
											
											if($preco_result)
											{
												//$tipo_pedido = 'natal_ima';
												$tipo_pedido = 'reposicao_ima';
												$valor_unitario = $preco_result[0]->valor;
												$valor_custo = $preco_result[0]->custo;
												
												if($tipo_pedido_preco=='reposicao')
												{
													$valor_custo = $preco_result[0]->custo_reposicao;	
												}
												
												$produto_descricao = "Acabamento: Imã \nTamanho: (Quadrado) 15cm x 15cm";
												
												$inserir_produto = true;
											}
											else
											{
												$data_error = array('produto_id' => $produto_id,
																	'produto_nome' => $produto_nome,
																	'acabamento_id' => 7,
																	'tamanho_id' => 43);
												
												array_push($error_inserir_produto, $data_error);
											}										
											
										}
										
										if($size == "filete20x20")
										{										
											$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(4), array(59));
											
											if($preco_result)
											{
												//$tipo_pedido = 'natal_filete';
												$tipo_pedido = 'reposicao_filete';
												$valor_unitario = $preco_result[0]->valor;
												$valor_custo = $preco_result[0]->custo;
												
												if($tipo_pedido_preco=='reposicao')
												{
													$valor_custo = $preco_result[0]->custo_reposicao;
												}
												
												$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Quadrado) 20cm x 20cm";
												
												$inserir_produto = true;
											}
											else
											{
												$data_error = array('produto_id' => $produto_id,
																	'produto_nome' => $produto_nome,
																	'acabamento_id' => 4,
																	'tamanho_id' => 59);
												
												array_push($error_inserir_produto, $data_error);
											}										
											
										}
										
										if($size == "matte20x20")
										{
											$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(3), array(59));
											
											if($preco_result)
											{
												//$tipo_pedido = 'natal_poster';
												$tipo_pedido = 'reposicao_poster';
												$valor_unitario = $preco_result[0]->valor;
												$valor_custo = $preco_result[0]->custo;
												
												if($tipo_pedido_preco=='reposicao')
												{
													$valor_custo = $preco_result[0]->custo_reposicao;
												}
												
												$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Quadrado) 20cm x 20cm";
												
												$inserir_produto = true;
											}
											else
											{
												$data_error = array('produto_id' => $produto_id,
																	'produto_nome' => $produto_nome,
																	'acabamento_id' => 3,
																	'tamanho_id' => 59);
												
												array_push($error_inserir_produto, $data_error);
											}										
											
										}									
										
										if($inserir_produto)
										{
											$foto = $produto_imagem;
											
											$quantidade = $quantidade_xls;
											
											$valor_artista = ($valor_unitario * 0.1 * $quantidade);
											// $valor_marketing = ($valor_unitario * 0.02 * $quantidade);
											// $valor_royalties = ($valor_unitario * 0.18 * $quantidade);
											
											$desconto_unitario = 0.00;
											$total = ($valor_unitario * $quantidade);
											$total_custo = ($valor_custo * $quantidade);
											//$total_custo_producao = ($valor_custo * $quantidade + $valor_artista + $valor_marketing + $valor_royalties);
											$total_custo_producao = ($valor_custo * $quantidade + $valor_artista);
											
											$valor_total_pedido = $valor_total_pedido + $total;
											$valor_total_custo_pedido = $valor_total_custo_pedido + $total_custo_producao;
											
											$artista = $artista_nome;
											
											$data_item = array('pedido_id' => $pedido_id,
															   'produto_id' => $produto_id,
															   'produto' => $produto_nome,
															   'descricao' => $produto_descricao,
															   'foto' => $foto,
															   'valor_unitario' => $valor_unitario,
															   'valor_custo' => $valor_custo,
															   'valor_artista' => $valor_artista,
															   'quantidade' => $quantidade,
															   'desconto_unitario' => $desconto_unitario,
															   'total' => $total,
															   'total_custo' => $total_custo,
															   'total_custo_producao' => $total_custo_producao,
															   'artista' => $artista_nome);
											
											$pedido_item_id = $this->pedidos_model->set($data_item, 'pedidos_pedidoitem');
										}
										
										if(sizeof($error_inserir_produto)>0)
										{
											echo '<pre>';
											print_r($error_inserir_produto);
										}
									}
								}
							}
						}					
					}
					
					$linha++;
				}
				
				$total_venda_pedido = round($valor_total_pedido);
				$total_custo_pedido = round($valor_total_custo_pedido);
				
				$data_update = array('tipo_pedido' => $tipo_pedido,
									 'total_itens' => $total_venda_pedido,
									 'total' => $total_venda_pedido,
									 'total_custo' => $total_custo_pedido);
				
				$this->pedidos_model->update($pedido_id, $data_update);				
				
			}				
		}		
	}
	
	public function executa_robo()
	{
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'xls|xlsx';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = FALSE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('planilha_excel'))
		{
			$this->session->set_flashdata('error_upload', strip_tags($this->upload->display_errors()));
			redirect('/pedidos/novo');
		}
		else
		{
			// importa a planilha
			$inputFileName = $_FILES['planilha_excel']['name'];
			//$inputFileName = 'Robô - Pedido Natal 2016 - Boa Viagem.xlsx';
			$inputFileType = 'Excel2007';
			$upload_path = './upload/';
			
			$this->load->library('PHPExcel');
			$inputFileType = PHPExcel_IOFactory::identify($upload_path . $inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			
			$arr_data = array();
			
			$objPHPExcel        = $objReader->load($upload_path . $inputFileName);
			$total_sheets       = $objPHPExcel->getSheetCount(); // here 4
			$allSheetName       = $objPHPExcel->getSheetNames(); // array ([0]=>'student',[1]=>'teacher',[2]=>'school',[3]=>'college')
			
			for ($sheet = 0; $sheet <= ($total_sheets-1); $sheet++)
			{				
				if($sheet >= 1) //inicio aba 2
				{			
					$objWorksheet       = $objPHPExcel->setActiveSheetIndex($sheet);
					$highestRow         = $objWorksheet->getHighestRow(); // here 5
					$highestColumn      = $objWorksheet->getHighestColumn(); // here 'E'
					$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  // here 5
					
					for ($row = 1; $row <= $highestRow; ++$row)
					{
						for ($col = 0; $col <= $highestColumnIndex; ++$col)
						{
							$value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
							
							if(is_array($arr_data))
							{
								$arr_data[$row-1][$col]=$value;
							}
						}
					}
					
					$loja_id = isset($_POST['loja']) ? $_POST['loja'] : false;
					$transportadora_id = isset($_POST['transportadora']) ? $_POST['transportadora'] : false;
					
					$error_pedido = array();
					
					if($loja_id)
					{
						$loja_result = $this->lojas_model->get_by(array('id' => $loja_id));
						
						if($loja_result)
						{
							$now = date("Y-m-d H:i:s");
							
							$cliente_id = $loja_result[0]->id;
							$cliente = $loja_result[0]->nome;
							$cpf = $loja_result[0]->cpf;
							$email = $loja_result[0]->email;
							$cep = $loja_result[0]->cep;
							$endereco = $loja_result[0]->endereco;
							$numero = $loja_result[0]->numero;
							$complemento = $loja_result[0]->complemento;
							$referencia = $loja_result[0]->referencia;
							$bairro = $loja_result[0]->bairro;
							$cidade = $loja_result[0]->cidade;
							$telefone = $loja_result[0]->telefone;
							$estado = $loja_result[0]->estado;
							
							$transportadora_result = $this->pedidos_model->get(array('razao_social'), 'fretes_transportadora', array('id' => $transportadora_id));
							
							if($transportadora_result)
							{
								$transportadora_nome = $transportadora_result[0]->razao_social;
							}							
							
							$data_pedido = array('data' => $now,
												 'fechamento' => $now,
												 'status' => 'AGUARDANDO PAGAMENTO',
												 'metodo_pagamento' => 'SHOPLINE',
												 'eh_poucanca' => 0,
												 'parcelas' => 1,
												 'tipo_servico' => '',
												 'peso_total' => 0.00,
												 'custo_envio' => 0.00,
												 'cliente_id' => $cliente_id,
												 'cliente' => $cliente,
												 'tipo_cliente' => 'PF',
												 'cpf' => $cpf,
												 'email' => $email,
												 'telefone' => $telefone,
												 'cobranca_cep' => $cep,
												 'cobranca_tipo_logradouro' => 'Residencial',
												 'cobranca_logradouro' => $endereco,
												 'cobranca_numero' => $numero,
												 'cobranca_complemento' => $complemento,
												 'cobranca_bairro' => $bairro,
												 'cobranca_cidade' => $cidade,
												 'cobranca_estado' => $estado,
												 'entrega_cep' => $cep,
												 'entrega_tipo_logradouro' => 'Residencial',
												 'entrega_logradouro' => $endereco,
												 'entrega_numero' => $numero,
												 'entrega_complemento' => $complemento,
												 'entrega_cidade' => $cidade,
												 'entrega_bairro' => $bairro,
												 'entrega_estado' => $estado,
												 'valor_cupom' => 0.00,
												 'total_itens' => 0.00,
												 'total' => 0.00,
												 'cupom_gerado' => 0,
												 'tipo_pedido' => '',
												 'tipo_unico' => 'loja',
												 'pedido_pai' => null,
												 'prazo_envio' => '2016-02-03 00:00:00',
												 'transportadora_id' => $transportadora_id,
												 'transportadora_razao_social' => $transportadora_nome);
							
							$pedido_id = $this->pedidos_model->set($data_pedido, 'pedidos_pedido');
						}				
					}
					else
					{
						$erro = 'Loja não encontrada.';
						array_push($error_pedido, $erro);
					}
					
					$linha = 1;
					
					$arr_size = array('matte30x30',
									  'matte36x475',
									  'matte475x625',
									  'filete30x30',
									  'filete36x475',
									  'filete475x625',
									  'ima15x15',
									  'filete20x20',
									  'matte20x20');
					
					$tipo_pedido_preco = 'reposicao';
					$tipo_pedido = 'reposicao';
					
					$valor_total_pedido = 0.00;
					$valor_total_custo_pedido = 0.00;
					
					foreach($arr_data as $data)
					{
						if($linha>8)
						{
							$arte_xls = addslashes($data[3]);
							
							$quantidade_filete30x30 = $data[10]; // Coluna K
							$quantidade_filete36x475 = $data[11]; // Coluna L
							$quantidade_filete475x625 = $data[12]; // Coluna M
							
							$quantidade_matte30x30 = $data[14]; // Coluna O
							$quantidade_matte36x475 = $data[15]; // Coluna P
							$quantidade_matte475x625 = $data[16]; // Coluna Q
							
							$quantidade_ima15x15 = $data[18]; // Coluna S
							
							$quantidade_filete20x20 = $data[23]; // Coluna X
							$quantidade_matte20x20 = $data[24]; // Coluna Y
							
							if(!empty($arte_xls))
							{
								foreach($arr_size as $size)
								{
									$quantidade_xls = ${"quantidade_$size"};
									
									if($quantidade_xls!="0" and $quantidade_xls!="")
									{
										// produto
										$produto_result = $this->produtos_model->get_utf8($arte_xls);
										
										if($produto_result)
										{
											$produto_id = $produto_result[0]->id;
											$produto_nome = addslashes($produto_result[0]->titulo_ptbr);
											$artista_id = $produto_result[0]->artista_id;
											$formato_id = $produto_result[0]->formato_id;
											
											// artista
											$artista_result = $this->lojas_model->get_artista(array('id' => $artista_id));
											
											if($artista_result)
											{
												$artista_nome = $artista_result[0]->nome;
											}
											
											// imagem
											$trabalho_result = $this->produtos_model->get(array('imagem'), 'trabalhos_trabalho', array('produto_id' => $produto_id));
											
											if($trabalho_result)
											{
												$produto_imagem = $trabalho_result[0]->imagem;
											}
											
											$inserir_produto = false;
											$error_inserir_produto = array();
											
											if($size == "filete30x30")
											{
												$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(4), array(17));
												
												if($preco_result)
												{
													$tipo_pedido_x = $tipo_pedido . '_filete';
													$valor_unitario = $preco_result[0]->valor;
													$valor_custo = $preco_result[0]->custo;
													
													if($tipo_pedido_preco=='reposicao')
													{
														$valor_custo = $preco_result[0]->custo_reposicao;
													}
													
													$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Quadrado) 30cm x 30cm";
													
													$inserir_produto = true;
												}
												else
												{
													$data_error = array('produto_id' => $produto_id,
																		'produto_nome' => $produto_nome,
																		'acabamento_id' => 4,
																		'tamanho_id' => 17);
													
													array_push($error_inserir_produto, $data_error);
												}
												
											}
											
											if($size == "filete36x475")
											{
												$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(4), array(1, 18));
												
												if($preco_result)
												{
													$tipo_pedido_x = $tipo_pedido . '_filete';
													$valor_unitario = $preco_result[0]->valor;
													$valor_custo = $preco_result[0]->custo;
													
													if($tipo_pedido_preco=='reposicao')
													{
														$valor_custo = $preco_result[0]->custo_reposicao;	
													}
													
													$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Retrato) 36cm x 47,5cm";
													
													if($formato_id == 2)
													{
													  $produto_descricao = "Acabamento: Poster Filete \nTamanho: (Paisagem) 47,5cm x 36cm";
													}
													
													$inserir_produto = true;
												}
												else
												{
													$data_error = array('produto_id' => $produto_id,
																		'produto_nome' => $produto_nome,
																		'acabamento_id' => 4,
																		'tamanho_id' => array(1, 18));
													
													array_push($error_inserir_produto, $data_error);
												}										
												
											}
											
											if($size == "filete475x625")
											{
												$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(4), array(2, 19));
												
												if($preco_result)
												{
													$tipo_pedido_x = $tipo_pedido . '_filete';
													$valor_unitario = $preco_result[0]->valor;
													$valor_custo = $preco_result[0]->custo;
													
													if($tipo_pedido_preco=='reposicao')
													{
														$valor_custo = $preco_result[0]->custo_reposicao;	
													}
													
													$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Retrato) 47,5cm x 62,5cm";
													
													if($formato_id == 2)
													{
														$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Paisagem) 62,5cm x 47,5cm";
													}
													
													$inserir_produto = true;
												}
												else
												{
													$data_error = array('produto_id' => $produto_id,
																		'produto_nome' => $produto_nome,
																		'acabamento_id' => 4,
																		'tamanho_id' => array(2, 19));
													
													array_push($error_inserir_produto, $data_error);
												}										
												
											}
											
											if($size == "matte30x30")
											{										
												$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(3), array(17));
												
												if($preco_result)
												{
													$tipo_pedido_x = $tipo_pedido . '_poster';
													$valor_unitario = $preco_result[0]->valor;
													$valor_custo = $preco_result[0]->custo;
													
													if($tipo_pedido_preco=='reposicao')
													{
														$valor_custo = $preco_result[0]->custo_reposicao;	
													}
													
													$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Quadrado) 30cm x 30cm";
													
													$inserir_produto = true;
												}
												else
												{
													$data_error = array('produto_id' => $produto_id,
																		'produto_nome' => $produto_nome,
																		'acabamento_id' => 3,
																		'tamanho_id' => 17);
													
													array_push($error_inserir_produto, $data_error);
												}										
												
											}
											
											if($size == "matte36x475")
											{
												$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(3), array(1, 18));
												
												if($preco_result)
												{
													$tipo_pedido_x = $tipo_pedido . '_poster';
													$valor_unitario = $preco_result[0]->valor;
													$valor_custo = $preco_result[0]->custo;
													
													if($tipo_pedido_preco=='reposicao')
													{
														$valor_custo = $preco_result[0]->custo_reposicao;	
													}
													
													$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Retrato) 36cm x 47,5cm";
													
													if($formato_id == 2)
													{
														$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Paisagem) 47,5cm x 36cm";
													}
													
													$inserir_produto = true;
												}
												else
												{
													$data_error = array('produto_id' => $produto_id,
																		'produto_nome' => $produto_nome,
																		'acabamento_id' => 3,
																		'tamanho_id' => array(1, 18));
													
													array_push($error_inserir_produto, $data_error);
												}										
												
											}
											
											if($size == "matte475x625")
											{
												$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(3), array(2, 19));
												
												if($preco_result)
												{
													$tipo_pedido_x = $tipo_pedido . '_poster';
													$valor_unitario = $preco_result[0]->valor;
													$valor_custo = $preco_result[0]->custo;
													
													if($tipo_pedido_preco=='reposicao')
													{
														$valor_custo = $preco_result[0]->custo_reposicao;	
													}
													
													$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Retrato) 47,5cm x 62,5cm";
													
													if($formato_id == 2)
													{
														$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Paisagem) 62,5cm x 47,5cm";
													}
													
													$inserir_produto = true;
												}
												else
												{
													$data_error = array('produto_id' => $produto_id,
																		'produto_nome' => $produto_nome,
																		'acabamento_id' => 3,
																		'tamanho_id' => array(2, 19));
													
													array_push($error_inserir_produto, $data_error);
												}										
												
											}
											
											if($size == "ima15x15")
											{										
												$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(7), array(43));
												
												if($preco_result)
												{
													$tipo_pedido_x = $tipo_pedido . '_ima';
													$valor_unitario = $preco_result[0]->valor;
													$valor_custo = $preco_result[0]->custo;
													
													if($tipo_pedido_preco=='reposicao')
													{
														$valor_custo = $preco_result[0]->custo_reposicao;	
													}
													
													$produto_descricao = "Acabamento: Imã \nTamanho: (Quadrado) 15cm x 15cm";
													
													$inserir_produto = true;
												}
												else
												{
													$data_error = array('produto_id' => $produto_id,
																		'produto_nome' => $produto_nome,
																		'acabamento_id' => 7,
																		'tamanho_id' => 43);
													
													array_push($error_inserir_produto, $data_error);
												}										
												
											}
											
											if($size == "filete20x20")
											{										
												$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(4), array(59));
												
												if($preco_result)
												{
													$tipo_pedido_x = $tipo_pedido . '_filete';
													$valor_unitario = $preco_result[0]->valor;
													$valor_custo = $preco_result[0]->custo;
													
													if($tipo_pedido_preco=='reposicao')
													{
														$valor_custo = $preco_result[0]->custo_reposicao;
													}
													
													$produto_descricao = "Acabamento: Poster Filete \nTamanho: (Quadrado) 20cm x 20cm";
													
													$inserir_produto = true;
												}
												else
												{
													$data_error = array('produto_id' => $produto_id,
																		'produto_nome' => $produto_nome,
																		'acabamento_id' => 4,
																		'tamanho_id' => 59);
													
													array_push($error_inserir_produto, $data_error);
												}										
												
											}
											
											if($size == "matte20x20")
											{										
												$preco_result = $this->produtos_model->get_preco(array('produto_id' => $produto_id), array(3), array(59));
												
												if($preco_result)
												{
													$tipo_pedido_x = $tipo_pedido . '_poster';
													$valor_unitario = $preco_result[0]->valor;
													$valor_custo = $preco_result[0]->custo;
													
													if($tipo_pedido_preco=='reposicao')
													{
														$valor_custo = $preco_result[0]->custo_reposicao;
													}
													
													$produto_descricao = "Acabamento: Papel Matte \nTamanho: (Quadrado) 20cm x 20cm";
													
													$inserir_produto = true;
												}
												else
												{
													$data_error = array('produto_id' => $produto_id,
																		'produto_nome' => $produto_nome,
																		'acabamento_id' => 3,
																		'tamanho_id' => 59);
													
													array_push($error_inserir_produto, $data_error);
												}										
												
											}									
											
											if($inserir_produto)
											{
												$foto = $produto_imagem;
												
												$quantidade = $quantidade_xls;
												
												$valor_artista = ($valor_unitario * 0.1 * $quantidade);
												// $valor_marketing = ($valor_unitario * 0.02 * $quantidade);
												// $valor_royalties = ($valor_unitario * 0.18 * $quantidade);
												
												$desconto_unitario = 0.00;
												$total = ($valor_unitario * $quantidade);
												$total_custo = ($valor_custo * $quantidade);
												//$total_custo_producao = ($valor_custo * $quantidade + $valor_artista + $valor_marketing + $valor_royalties);
												$total_custo_producao = ($valor_custo * $quantidade + $valor_artista);
												
												$valor_total_pedido = $valor_total_pedido + $total;
												$valor_total_custo_pedido = $valor_total_custo_pedido + $total_custo_producao;
												
												$artista = $artista_nome;
												
												$data_item = array('pedido_id' => $pedido_id,
																   'produto_id' => $produto_id,
																   'produto' => $produto_nome,
																   'descricao' => utf8_decode($produto_descricao),
																   'foto' => $foto,
																   'valor_unitario' => $valor_unitario,
																   'valor_custo' => $valor_custo,
																   'valor_artista' => $valor_artista,
																   'quantidade' => $quantidade,
																   'desconto_unitario' => $desconto_unitario,
																   'total' => $total,
																   'total_custo' => $total_custo,
																   'total_custo_producao' => $total_custo_producao,
																   'artista' => $artista_nome);
												
												$pedido_item_id = $this->pedidos_model->set($data_item, 'pedidos_pedidoitem');
											}
											
											if(sizeof($error_inserir_produto)>0)
											{
												echo '<pre>';
												print_r($error_inserir_produto);
											}
										}
									}
								}
							}
						}
						
						$linha++;
					}
					
					$total_venda_pedido = round($valor_total_pedido);
					$total_custo_pedido = round($valor_total_custo_pedido);
					
					$data_update = array('tipo_pedido' => $tipo_pedido_x,
										 'total_itens' => $total_venda_pedido,
										 'total' => $total_venda_pedido,
										 'total_custo' => $total_custo_pedido);
					
					$this->pedidos_model->update($pedido_id, $data_update);
					
				}
			}
		}
	}
	
	public function terceirizar()
	{
		$data_select = array('id', 'tipo_pedido', 'status', 'cliente');
		$data_where = array('tipo_pedido' => 'natal_poster');
		$data_limit = 50;
		
		if($_POST)
		{
			$tipo_pedido = $_POST['tipo_pedido'];
			$loja = $_POST['loja'];
			$status = $_POST['status'];
			
			$data_where = array('tipo_pedido' => $tipo_pedido,
								'cliente_id' => $loja,
								'status' => $status);
			
			if($tipo_pedido==''){
				unset($data_where['tipo_pedido']);
			}
			
			if($loja==''){
				unset($data_where['cliente_id']);
			}
			
			if($status==''){
				unset($data_where['status']);
			}			
		}		
		
		$pedidos = $this->pedidos_model->get($data_select, false, $data_where, $data_limit);
		$lojas = $this->lojas_model->get();
		$pedido_tipo = $this->pedidos_model->get_group_by(array('id', 'tipo_pedido'), 'pedidos_pedido', 'tipo_pedido <> ""', 'tipo_pedido');
		$pedido_status = $this->pedidos_model->get_group_by(array('id', 'status'), 'pedidos_pedido', 'status <> ""', 'status');
		
		$data = array('usuario_sessao' => $this->usuario_sessao,
					  'pedidos' => $pedidos,
					  'lojas' => $lojas,
					  'pedido_tipo' => $pedido_tipo,
					  'pedido_status' => $pedido_status);
		
		$this->load->view('pedidos_terceirizar', $data);
	}
	
	public function exporta_terceirizacao()
	{
		if($_POST)
		{
			$sheet = new PHPExcel();
			$sheet->getProperties()->setTitle('Lote')->setDescription('Lote');
			
			$pedido_ids = $_POST['pedido_id'];
			
			$itens_007_043 = array();
			
			$itens_003_018 = array();
			$itens_003_019 = array();
			$itens_003_059 = array();
			$itens_003_017 = array();
			$itens_003_001 = array();
			$itens_003_002 = array();
			
			$itens_004_018 = array();
			$itens_004_019 = array();
			$itens_004_059 = array();
			$itens_004_017 = array();
			$itens_004_001 = array();
			$itens_004_002 = array();
			
			foreach($pedido_ids as $pedido_id):
				
				$data_select = array('id', 'pedido_id', 'produto_id', 'produto', 'tiragem', 'acabamento_id', 'tamanho_id', 'quantidade');
				$data_where = array('pedido_id' => $pedido_id);
				$data_acabamento = array(3, 4, 7);
				$data_tamanho = array(1, 2, 8, 19, 20, 7, 96);
				
				$pedido_itens = $this->pedidos_model->get_item($data_select, $data_where, $data_acabamento, $data_tamanho);
				
				foreach($pedido_itens as $pedido_item):
					
					$item_id = $pedido_item->id;
					$pedido_id = $pedido_item->pedido_id;
					$produto_id = $pedido_item->produto_id;
					$produto = $pedido_item->produto;
					$tiragem = $pedido_item->tiragem;
					$acabamento_id = $pedido_item->acabamento_id;
					$tamanho_id = $pedido_item->tamanho_id;
					$quantidade = $pedido_item->quantidade;
					
					// artista
					$artista_result = $this->lojas_model->get_artista_by_produto(array('id' => $produto_id));
					
					if($artista_result)
					{
						$artista_id = $artista_result[0]->artista_id;
						$artista_result2 = $this->lojas_model->get_artista(array('id' => $artista_id));
						
						if($artista_result2)
						{
							$artista_nome = $artista_result2[0]->nome;
						}
					}
					
					$artista_id_formato = str_pad($artista_id, 6, '0', STR_PAD_LEFT);
					$produto_id_formato = str_pad($produto_id, 6, '0', STR_PAD_LEFT);
					$acabamento_formato = str_pad($acabamento_id, 3, '0', STR_PAD_LEFT);
					$tamanho_formato = str_pad($tamanho_id, 3, '0', STR_PAD_LEFT);
					
					$codigo = $produto_id_formato .'_'. $artista_id_formato;
					
					$data_item = array('item_id' => $item_id,
									   'codigo' => $codigo,
									   'pedido_id' => $pedido_id,
									   'produto_id' => $produto_id_formato,
									   'produto' => $produto,
									   'tiragem' => $tiragem,
									   'acabamento_id' => $acabamento_formato,
									   'tamanho_id' => $tamanho_formato,
									   'quantidade' => $quantidade,
									   'artista' => $artista_nome);
					
					#ima
					if($acabamento_id == 7 && $tamanho_id == 7)
					{
						array_push($itens_007_043, $data_item);
					}
					
					#poster
					if($acabamento_id == 3 && $tamanho_id == 19)
					{
						array_push($itens_003_018, $data_item);
					}
					
					if($acabamento_id == 3 && $tamanho_id == 20)
					{
						array_push($itens_003_019, $data_item);
					}
					
					if($acabamento_id == 3 && $tamanho_id == 96)
					{
						array_push($itens_003_059, $data_item);
					}
					
					if($acabamento_id == 3 && $tamanho_id == 8)
					{
						array_push($itens_003_017, $data_item);
					}
					
					if($acabamento_id == 3 && $tamanho_id == 1)
					{
						array_push($itens_003_001, $data_item);
					}
					
					if($acabamento_id == 3 && $tamanho_id == 2)
					{
						array_push($itens_003_002, $data_item);
					}
					
					#filete
					if($acabamento_id == 4 && $tamanho_id == 19)
					{
						array_push($itens_004_018, $data_item);
					}
					
					if($acabamento_id == 4 && $tamanho_id == 20)
					{
						array_push($itens_004_019, $data_item);
					}
					
					if($acabamento_id == 4 && $tamanho_id == 96)
					{
						array_push($itens_004_059, $data_item);
					}
					
					if($acabamento_id == 4 && $tamanho_id == 8)
					{
						array_push($itens_004_017, $data_item);
					}
					
					if($acabamento_id == 4 && $tamanho_id == 1)
					{
						array_push($itens_004_001, $data_item);
					}
					
					if($acabamento_id == 4 && $tamanho_id == 2)
					{
						array_push($itens_004_002, $data_item);
					}					
				
				endforeach;
				
			endforeach;
			
			$sheet_007_043 = false;
			
			$sheet_003_018 = false;
			$sheet_003_019 = false;
			$sheet_003_059 = false;
			$sheet_003_017 = false;
			$sheet_003_001 = false;
			$sheet_003_002 = false;
			
			$sheet_004_018 = false;
			$sheet_004_019 = false;
			$sheet_004_059 = false;
			$sheet_004_017 = false;
			$sheet_004_001 = false;
			$sheet_004_002 = false;
			
			$sheet_index = 0;
			
			// 007_043
			if(sizeof($itens_007_043)>0)
			{
				$sheet_007_043 = true;
				
				$sheet->setActiveSheetIndex(0)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet(0)->setTitle('007_043');
				
				$i = 2;
				
				foreach($itens_007_043 as $item_007_043):
					
					$quantidade = $item_007_043['quantidade'];
					
					for ($x = 1; $x <= $quantidade; $x++):
						
						$sheet->setActiveSheetIndex(0)
						->setCellValue('A'.$i, $item_007_043['codigo'])
						->setCellValueExplicit('B'.$i, $item_007_043['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '043', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_007_043['pedido_id'])
						->setCellValue('E'.$i, '')
						->setCellValue('F'.$i, $item_007_043['produto'])
						->setCellValue('G'.$i, $item_007_043['artista']);
						
						$i++;
					
					endfor;
					
				endforeach;
			}
			
			// 003_018
			if(sizeof($itens_003_018)>0)
			{
				$sheet_003_018 = true;
				
				if($sheet_007_043)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('003_018');
				
				$i = 2;
				
				foreach($itens_003_018 as $item_003_018):
				
					$quantidade = $item_003_018['quantidade'];
					
					$exp_tiragem = false;
					
					if($quantidade<>1)
					{
						$exp_tiragem = true;
						$arr_tiragem = explode(';', $item_003_018['tiragem']);
					}
					
					for ($x = 1; $x <= $quantidade; $x++):
						
						$tiragem = $item_003_018['tiragem'];
						
						if($exp_tiragem)
						{
							$tiragem = $arr_tiragem[($x-1)];
						}
						
						$tiragem = str_replace(';', '', $tiragem);
						
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_003_018['codigo'])
						->setCellValueExplicit('B'.$i, $item_003_018['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '018', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_003_018['pedido_id'])
						->setCellValue('E'.$i, $tiragem)
						->setCellValue('F'.$i, $item_003_018['produto'])
						->setCellValue('G'.$i, $item_003_018['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}
			
			// 003_019
			if(sizeof($itens_003_019)>0)
			{
				$sheet_003_019 = true;
				
				if($sheet_007_043 || $sheet_003_018)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('003_019');
				
				$i = 2;
				
				foreach($itens_003_019 as $item_003_019):
				
					$quantidade = $item_003_019['quantidade'];
					
					$exp_tiragem = false;
					
					if($quantidade<>1)
					{
						$exp_tiragem = true;
						$arr_tiragem = explode(';', $item_003_019['tiragem']);
					}
					
					for ($x = 1; $x <= $quantidade; $x++):
						
						$tiragem = $item_003_019['tiragem'];
						
						if($exp_tiragem)
						{
							$tiragem = $arr_tiragem[($x-1)];
						}
						
						$tiragem = str_replace(';', '', $tiragem);
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_003_019['codigo'])
						->setCellValueExplicit('B'.$i, $item_003_019['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '019', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_003_019['pedido_id'])
						->setCellValue('E'.$i, $tiragem)
						->setCellValue('F'.$i, $item_003_019['produto'])
						->setCellValue('G'.$i, $item_003_019['artista']);
						
						$i++;
					
					endfor;
					
				endforeach;
			}

			// 003_059
			if(sizeof($itens_003_059)>0)
			{
				$sheet_003_059 = true;
				
				if($sheet_007_043 || $sheet_003_018 || $sheet_003_019)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('003_059');
				
				$i = 2;
				
				foreach($itens_003_059 as $item_003_059):
				
					$quantidade = $item_003_059['quantidade'];
					
					for ($x = 1; $x <= $quantidade; $x++):
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_003_059['codigo'])
						->setCellValueExplicit('B'.$i, $item_003_059['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '059', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_003_059['pedido_id'])
						->setCellValue('E'.$i, '')
						->setCellValue('F'.$i, $item_003_059['produto'])
						->setCellValue('G'.$i, $item_003_059['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}
			
			// 003_017
			if(sizeof($itens_003_017)>0)
			{
				$sheet_003_017 = true;
				
				if($sheet_003_059 || $sheet_003_019 || $sheet_003_018)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('003_017');
				
				$i = 2;
				
				foreach($itens_003_017 as $item_003_017):
				
					$quantidade = $item_003_017['quantidade'];
					
					for ($x = 1; $x <= $quantidade; $x++):
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_003_017['codigo'])
						->setCellValueExplicit('B'.$i, $item_003_017['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '017', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_003_017['pedido_id'])
						->setCellValue('E'.$i, '')
						->setCellValue('F'.$i, $item_003_017['produto'])
						->setCellValue('G'.$i, $item_003_017['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}			
			
			// 003_001
			if(sizeof($itens_003_001)>0)
			{
				$sheet_003_001 = true;
				
				if($sheet_003_059 || $sheet_003_019 || $sheet_003_018 || $sheet_003_017)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('003_001');
				
				$i = 2;
				
				foreach($itens_003_001 as $item_003_001):
				
					$quantidade = $item_003_001['quantidade'];
					
					$exp_tiragem = false;
					
					if($quantidade>1)
					{
						if(strpos($item_003_001['tiragem'], ';') !== false)
						{
							$arr_tiragem = explode(';', $item_003_001['tiragem']);
							$exp_tiragem = true;
						}
					}
					
					for ($x = 1; $x <= $quantidade; $x++):
						
						$tiragem = $item_003_001['tiragem'];
						
						if($exp_tiragem)
						{
							$tiragem = $arr_tiragem[($x-1)];
						}
						
						$tiragem = str_replace(';', '', $tiragem);
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_003_001['codigo'])
						->setCellValueExplicit('B'.$i, $item_003_001['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '001', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_003_001['pedido_id'])
						->setCellValue('E'.$i, $tiragem)
						->setCellValue('F'.$i, $item_003_001['produto'])
						->setCellValue('G'.$i, $item_003_001['artista']);
						
						$i++;
					
					endfor;
					
				endforeach;
			}

			// 003_002
			if(sizeof($itens_003_002)>0)
			{
				$sheet_003_002 = true;
				
				if($sheet_003_059 || $sheet_003_019 || $sheet_003_018 || $sheet_003_017 || $sheet_003_001)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('003_002');
				
				$i = 2;
				
				foreach($itens_003_002 as $item_003_002):
				
					$quantidade = $item_003_002['quantidade'];
					
					$exp_tiragem = false;
					
					if($quantidade>1)
					{
						if(strpos($item_003_002['tiragem'], ';') !== false)
						{
							$arr_tiragem = explode(';', $item_003_002['tiragem']);
							$exp_tiragem = true;
						}
					}
					
					for ($x = 1; $x <= $quantidade; $x++):
						
						$tiragem = $item_003_002['tiragem'];
						
						if($exp_tiragem)
						{
							$tiragem = $arr_tiragem[($x-1)];
						}
						
						$tiragem = str_replace(';', '', $tiragem);
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_003_002['codigo'])
						->setCellValueExplicit('B'.$i, $item_003_002['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '002', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_003_002['pedido_id'])
						->setCellValue('E'.$i, $tiragem)
						->setCellValue('F'.$i, $item_003_002['produto'])
						->setCellValue('G'.$i, $item_003_002['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}
			
			// 004_018
			if(sizeof($itens_004_018)>0)
			{
				$sheet_004_018 = true;
				
				if($sheet_003_059 || $sheet_003_019 || $sheet_003_018 || $sheet_003_017 || $sheet_003_001 || $sheet_003_002)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('004_018');
				
				$i = 2;
				
				foreach($itens_004_018 as $item_004_018):
				
					$quantidade = $item_004_018['quantidade'];
					
					$exp_tiragem = false;
					
					if($quantidade>1)
					{
						if(strpos($item_004_018['tiragem'], ';') !== false)
						{
							$arr_tiragem = explode(';', $item_004_018['tiragem']);
							$exp_tiragem = true;
						}
					}
					
					for ($x = 1; $x <= $quantidade; $x++):
						
						$tiragem = $item_004_018['tiragem'];
						
						if($exp_tiragem)
						{
							$tiragem = $arr_tiragem[($x-1)];
						}
						
						$tiragem = str_replace(';', '', $tiragem);
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_004_018['codigo'])
						->setCellValueExplicit('B'.$i, $item_004_018['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '018', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_004_018['pedido_id'])
						->setCellValue('E'.$i, $tiragem)
						->setCellValue('F'.$i, $item_004_018['produto'])
						->setCellValue('G'.$i, $item_004_018['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}
			
			// 004_019
			if(sizeof($itens_004_019)>0)
			{
				$sheet_004_019 = true;
				
				if($sheet_003_059 || $sheet_003_019 || $sheet_003_018 || $sheet_003_017 || $sheet_003_001 || $sheet_003_002 || $sheet_004_018)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('004_019');
				
				$i = 2;
				
				foreach($itens_004_019 as $item_004_019):
				
					$quantidade = $item_004_019['quantidade'];
					
					$exp_tiragem = false;
					
					if($quantidade>1)
					{
						if(strpos($item_004_019['tiragem'], ';') !== false)
						{
							$arr_tiragem = explode(';', $item_004_019['tiragem']);
							$exp_tiragem = true;
						}
					}
					
					for ($x = 1; $x <= $quantidade; $x++):
						
						$tiragem = $item_004_019['tiragem'];
						
						if($exp_tiragem)
						{
							$tiragem = $arr_tiragem[($x-1)];
						}
						
						$tiragem = str_replace(';', '', $tiragem);
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_004_019['codigo'])
						->setCellValueExplicit('B'.$i, $item_004_019['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '019', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_004_019['pedido_id'])
						->setCellValue('E'.$i, $tiragem)
						->setCellValue('F'.$i, $item_004_019['produto'])
						->setCellValue('G'.$i, $item_004_019['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}
			
			// 004_059
			if(sizeof($itens_004_059)>0)
			{
				$sheet_004_059 = true;
				
				if($sheet_003_059 || $sheet_003_019 || $sheet_003_018 || $sheet_003_017 || $sheet_003_001 || $sheet_003_002 || $sheet_004_018 || $sheet_004_019)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('004_059');
				
				$i = 2;
				
				foreach($itens_004_059 as $item_004_059):
				
					$quantidade = $item_004_059['quantidade'];
					
					for ($x = 1; $x <= $quantidade; $x++):
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_004_059['codigo'])
						->setCellValueExplicit('B'.$i, $item_004_059['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '059', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_004_059['pedido_id'])
						->setCellValue('E'.$i, '')
						->setCellValue('F'.$i, $item_004_059['produto'])
						->setCellValue('G'.$i, $item_004_059['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}
			
			// 004_017
			if(sizeof($itens_004_017)>0)
			{
				$sheet_004_017 = true;
				
				if($sheet_003_059 || $sheet_003_019 || $sheet_003_018 || $sheet_003_017 || $sheet_003_001 || $sheet_003_002 || $sheet_004_018 || $sheet_004_019 || $sheet_004_059)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('004_017');
				
				$i = 2;
				
				foreach($itens_004_017 as $item_004_017):
				
					$quantidade = $item_004_017['quantidade'];
					
					for ($x = 1; $x <= $quantidade; $x++):
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_004_017['codigo'])
						->setCellValueExplicit('B'.$i, $item_004_017['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '017', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_004_017['pedido_id'])
						->setCellValue('E'.$i, '')
						->setCellValue('F'.$i, $item_004_017['produto'])
						->setCellValue('G'.$i, $item_004_017['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}
			
			// 004_001
			if(sizeof($itens_004_001)>0)
			{
				$sheet_004_001 = true;
				
				if($sheet_003_059 || $sheet_003_019 || $sheet_003_018 || $sheet_003_017 || $sheet_003_001 || $sheet_003_002 || $sheet_004_018 || $sheet_004_019 || $sheet_004_059 || $sheet_004_017)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('004_001');
				
				$i = 2;
				
				foreach($itens_004_001 as $item_004_001):
				
					$quantidade = $item_004_001['quantidade'];
					
					$exp_tiragem = false;
					
					if($quantidade>1)
					{
						if(strpos($item_004_001['tiragem'], ';') !== false)
						{
							$arr_tiragem = explode(';', $item_004_001['tiragem']);
							$exp_tiragem = true;
						}
					}
					
					for ($x = 1; $x <= $quantidade; $x++):
						
						$tiragem = $item_004_001['tiragem'];
						
						if($exp_tiragem)
						{
							$tiragem = $arr_tiragem[($x-1)];
						}
						
						$tiragem = str_replace(';', '', $tiragem);
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_004_001['codigo'])
						->setCellValueExplicit('B'.$i, $item_004_001['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '001', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_004_001['pedido_id'])
						->setCellValue('E'.$i, $tiragem)
						->setCellValue('F'.$i, $item_004_001['produto'])
						->setCellValue('G'.$i, $item_004_001['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}
			
			// 004_002
			if(sizeof($itens_004_002)>0)
			{
				$sheet_004_002 = true;
				
				if($sheet_003_059 || $sheet_003_019 || $sheet_003_018 || $sheet_003_017 || $sheet_003_001 || $sheet_003_002 || $sheet_004_018 || $sheet_004_019 || $sheet_004_059 || $sheet_004_017 || $sheet_004_001)
				{
					$sheet->createSheet();
					$sheet_index++;
				}
				
				$sheet->setActiveSheetIndex($sheet_index)
				->setCellValue('A1', 'Codigo')
				->setCellValue('B1', 'Acabamento')
				->setCellValue('C1', 'Formato')
				->setCellValue('D1', 'Pedido')
				->setCellValue('E1', 'Certificado')
				->setCellValue('F1', 'Produto')
				->setCellValue('G1', 'Artista');
				
				$sheet->getActiveSheet($sheet_index)->setTitle('004_002');
				
				$i = 2;
				
				foreach($itens_004_002 as $item_004_002):
				
					$quantidade = $item_004_002['quantidade'];
					
					$exp_tiragem = false;
					
					if($quantidade>1)
					{
						if(strpos($item_004_002['tiragem'], ';') !== false)
						{
							$arr_tiragem = explode(';', $item_004_001['tiragem']);
							$exp_tiragem = true;
						}
					}
					
					for ($x = 1; $x <= $quantidade; $x++):
						
						$tiragem = $item_004_002['tiragem'];
						
						if($exp_tiragem)
						{
							$tiragem = $arr_tiragem[($x-1)];
						}
						
						$tiragem = str_replace(';', '', $tiragem);
					
						$sheet->setActiveSheetIndex($sheet_index)
						->setCellValue('A'.$i, $item_004_002['codigo'])
						->setCellValueExplicit('B'.$i, $item_004_002['acabamento_id'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('C'.$i, '002', PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValue('D'.$i, $item_004_002['pedido_id'])
						->setCellValue('E'.$i, $tiragem)
						->setCellValue('F'.$i, $item_004_002['produto'])
						->setCellValue('G'.$i, $item_004_002['artista']);
						
						$i++;
						
					endfor;
					
				endforeach;
			}
			
			$lote_data = date('dm');
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="lote_'.$lote_data.'.xls"');
			header('Cache-Control: max-age=0');
			
			header('Cache-Control: max-age=1');
			
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			
			$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
			$objWriter->save('php://output');
			exit;
			
		}
	}
}