<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	public $usuario_sessao;
	
	public function __construct()
	{
		ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 30000);

		$CI =& get_instance();

		$CI->db = $CI->load->database('default', TRUE);
		$CI->db2 = $CI->load->database('db2', TRUE);

        if( ! ini_get('date.timezone') )
        {
            date_default_timezone_set('America/Sao_Paulo');
        }
		
		$this->load->library(array('session', 'pagination', 'email'));
		$this->load->helper(array('url', 'form', 'date'));

		$this->usuario_sessao = $this->session->userdata("usuario_sessao");

		if($this->uri->segment(1)=='relatorios'
		|| $this->uri->segment(1)=='pedidos')
		{
			if(empty($this->usuario_sessao))
			{
				redirect('/login/');
			}
		}
	}	
}